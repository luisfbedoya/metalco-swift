//
//  AppDelegate+Extension.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import Swinject
import SwinjectStoryboard

extension AppDelegate {
    func container() -> Container {
        let container = Container()
        //Navigation
        container.storyboardInitCompleted(MainNavigationViewController.self) { (r, c) in
            
        }
        container.storyboardInitCompleted(MainNavigationViewController.self, name: "workplace") { (r, c) in
            c.viewControllers = [r.resolve(WorkplaceViewController.self)!]
        }
        container.storyboardInitCompleted(MainNavigationViewController.self, name: "history") { (r, c) in
            c.viewControllers = [r.resolve(HistoryViewController.self)!]
        }
        container.storyboardInitCompleted(MainNavigationViewController.self, name: "profile") { (r, c) in
            c.viewControllers = [r.resolve(ProfileViewController.self)!]
        }
        
        container.storyboardInitCompleted(TabBarViewController.self) { (r, c) in
            let storyboard = SwinjectStoryboard.create(name: "Home", bundle: nil, container: container)
            let workplace = storyboard.instantiateViewController(withIdentifier: "workplace")
            let history = storyboard.instantiateViewController(withIdentifier: "history")
            let profile = storyboard.instantiateViewController(withIdentifier: "profile")
            c.viewControllers = [workplace, history, profile]
        }
        //Controllers
        container.storyboardInitCompleted(LoginViewController.self) { (r, c) in
            c.viewModel = r.resolve(LoginViewModel.self)
        }
        container.storyboardInitCompleted(RegisterViewController.self) { (r, c) in
            c.viewModel = r.resolve(RegisterViewModel.self)
        }
        container.storyboardInitCompleted(WorkplaceViewController.self) { (r, c) in
            c.viewModel = r.resolve(WorkplaceViewModel.self)
        }
        container.storyboardInitCompleted(HistoryViewController.self) { (r, c) in
            c.viewModel = r.resolve(HistoryViewModel.self)
        }
        container.storyboardInitCompleted(ProfileViewController.self) { (r, c) in
            c.viewModel = r.resolve(ProfileViewModel.self)
        }
        container.storyboardInitCompleted(LocationValidatorViewController.self) { (r, c) in
            c.viewModel = r.resolve(LocationValidatorViewModel.self)
        }
        container.storyboardInitCompleted(DashboardViewController.self) { (r, c) in
            c.viewModel = r.resolve(DashboardViewModel.self)
        }
        container.storyboardInitCompleted(ResourceMenuViewController.self) { (r, c) in
            c.viewModel = r.resolve(ResourceMenuViewModel.self)
        }
        container.storyboardInitCompleted(WarehouseViewController.self) { (r, c) in
            c.viewModel = r.resolve(WarehouseViewModel.self)
        }
        container.storyboardInitCompleted(ResourcesViewController.self) { (r, c) in
            c.viewModel = r.resolve(ResourcesViewModel.self)
        }
        //ViewModel
        container.register(LoginViewModel.self) { (r) -> LoginViewModel in
            LoginViewModel(useCase: r.resolve(NetworkAuthenticationUseCase.self)!, navigator: r.resolve(LoginNavigator.self)!)
        }
        container.register(RegisterViewModel.self) { (r) -> RegisterViewModel in
            RegisterViewModel(useCase: r.resolve(NetworkAuthenticationUseCase.self)!)
        }
        container.register(WorkplaceViewModel.self) { (r) -> WorkplaceViewModel in
            WorkplaceViewModel(useCase: r.resolve(NetworkWorkplaceUseCase.self)!, navigator: r.resolve(WorkplaceNavigator.self)!)
        }
        container.register(HistoryViewModel.self) { (r) -> HistoryViewModel in
            HistoryViewModel(useCase: r.resolve(NetworkHistoryUseCase.self)!)
        }
        container.register(ProfileViewModel.self) { (r) -> ProfileViewModel in
            ProfileViewModel(useCase: r.resolve(NetworkProfileUseCase.self)!, navigator: r.resolve(ProfileNavigator.self)!)
        }
        container.register(LocationValidatorViewModel.self) { (r) -> LocationValidatorViewModel in
            LocationValidatorViewModel(useCase: r.resolve(NetworkLocationValidationUseCase.self)!, navigator: r.resolve(LocationValidatorNavigator.self)!)
        }
        container.register(DashboardViewModel.self) { (r) -> DashboardViewModel in
            DashboardViewModel(useCase: r.resolve(NetworkDashboardUseCase.self)!, navigator: r.resolve(DashboardNavigator.self)!)
        }
        container.register(ResourceMenuViewModel.self) { (r) -> ResourceMenuViewModel in
            ResourceMenuViewModel(warehouseUseCase: r.resolve(NetworkWarehouseUseCase.self)!, navigator: r.resolve(ResourceMenuNavigator.self)!)
        }
        container.register(WarehouseViewModel.self) { (r) -> WarehouseViewModel in
            WarehouseViewModel(useCase: r.resolve(NetworkResourceUseCase.self)!, warehouseUseCase: r.resolve(NetworkWarehouseUseCase.self)!)
        }
        container.register(ResourcesViewModel.self) { (r) -> ResourcesViewModel in
            ResourcesViewModel(useCase: r.resolve(NetworkResourceUseCase.self)!, warehouseUseCase: r.resolve(NetworkWarehouseUseCase.self)!)
        }
        //Network
        container.register(NetworkAuthenticationUseCase.self) { (r) -> NetworkAuthenticationUseCase in
            (r.resolve(NetworkUseCaseProvider.self))!.makeAuthenticationUseCase() as! NetworkAuthenticationUseCase
        }
        container.register(NetworkHistoryUseCase.self) { (r) -> NetworkHistoryUseCase in
            (r.resolve(NetworkUseCaseProvider.self))!.makeHistoryUseCase() as! NetworkHistoryUseCase
        }
        container.register(NetworkProfileUseCase.self) { (r) -> NetworkProfileUseCase in
            (r.resolve(NetworkUseCaseProvider.self))!.makeProfileUseCase() as! NetworkProfileUseCase
        }
        container.register(NetworkWorkplaceUseCase.self) { (r) -> NetworkWorkplaceUseCase in
            (r.resolve(NetworkUseCaseProvider.self))!.makeWorkplaceUseCase() as! NetworkWorkplaceUseCase
        }
        container.register(NetworkLocationValidationUseCase.self) { (r) -> NetworkLocationValidationUseCase in
            (r.resolve(NetworkUseCaseProvider.self))!.makeLocationValidationUseCase() as! NetworkLocationValidationUseCase
        }
        container.register(NetworkDashboardUseCase.self) { (r) -> NetworkDashboardUseCase in
            (r.resolve(NetworkUseCaseProvider.self))!.makeDashboardUseCase() as! NetworkDashboardUseCase
        }
        container.register(NetworkWarehouseUseCase.self) { (r) -> NetworkWarehouseUseCase in
            (r.resolve(NetworkUseCaseProvider.self))!.makeWarehouseUseCase() as! NetworkWarehouseUseCase
        }
        container.register(NetworkResourceUseCase.self) { (r) -> NetworkResourceUseCase in
            (r.resolve(NetworkUseCaseProvider.self))!.makeResourceUseCase() as! NetworkResourceUseCase
        }
        container.register(NetworkUseCaseProvider.self) { (r) -> NetworkUseCaseProvider in
            NetworkUseCaseProvider()
        }
        //Navigators
        container.register(LoginNavigator.self) { (r) -> LoginNavigator in
            LoginNavigator(storyboard: SwinjectStoryboard.create(name: "Home", bundle: nil, container: container), dashboardStoryboard: SwinjectStoryboard.create(name: "Dashboard", bundle: nil, container: container), registerStoryboard: SwinjectStoryboard.create(name: "Authentication", bundle: nil, container: container))
        }
        container.register(ProfileNavigator.self) { (r) -> ProfileNavigator in
            ProfileNavigator(storyboard: SwinjectStoryboard.create(name: "Authentication", bundle: nil, container: container))
        }
        container.register(WorkplaceNavigator.self) { (r) -> WorkplaceNavigator in
            let storyboard = SwinjectStoryboard.create(name: "Home", bundle: nil, container: container)
            return WorkplaceNavigator(storyboard: storyboard)
        }
        container.register(LocationValidatorNavigator.self) { (r) -> LocationValidatorNavigator in
            LocationValidatorNavigator(storyboard: SwinjectStoryboard.create(name: "Dashboard", bundle: nil, container: container))
        }
        container.register(DashboardNavigator.self) { (r) -> DashboardNavigator in
            DashboardNavigator(storyboard: SwinjectStoryboard.create(name: "Home", bundle: nil, container: container))
        }
        container.register(ResourceMenuNavigator.self) { (r) -> ResourceMenuNavigator in
            ResourceMenuNavigator(storyboard: SwinjectStoryboard.create(name: "Home", bundle: nil, container: container))
        }
        return container
    }
}
