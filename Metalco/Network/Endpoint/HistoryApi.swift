//
//  HistoryApi.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public enum HistoryApi {
    case records(userId: String, workerId: String)
}

extension HistoryApi: EndPointType {
    public var task: HTTPTask {
        switch self {
        case .records(userId: let userId, workerId: let workerId):
            return .requestParameters(bodyParameters: ["userID":userId, "workerID": workerId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        }
    }
    
    public var method: HTTPMethod {
        return .post
    }
    
    
    public var path: String {
        switch self {
        case .records:
            return "records/byuser"
        }
    }
}

public class HistoryNetwork: NetworkOperation<RecordData, HistoryApi>  {
    var network: Router<HistoryApi>
    
    public init(network: Router<HistoryApi>) {
        self.network = network
    }
    
    func fetch(userId: String, workerId: String, onSuccess: (([Record]) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .records(userId: userId, workerId: workerId)
        execute(network: network, onSuccess: { data in
            onSuccess?(data.data)
        }, onError: onError)
    }
}
