//
//  AuthenticationApi.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

public enum AuthenticationApi {
    case login(username: String, password: String, token: String)
    case register(username: String, password: String, workerCode: String)
    case fetch(userId: String, token: String)
}

extension AuthenticationApi: EndPointType {
    public var task: HTTPTask {
        switch self {
        case .login(let username, let password, let token):
            return .requestParameters(bodyParameters: ["username":username, "password": password, "deviceID": token], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .register(let username, let password, let workerCode):
            return .requestParameters(bodyParameters: ["username":username, "password": password, "workerNumber": workerCode, "type": "WORKER"], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .fetch(let userId, let token):
            return .requestParameters(bodyParameters: ["userID": userId, "token": token], bodyEncoding: .jsonEncoding, urlParameters: nil)
        }
    }
    
    public var method: HTTPMethod {
        return .post
    }
    
    
    public var path: String {
        switch self {
        case .login:
            return "auth/login"
        case .register:
            return "users"
        case .fetch:
            return "users/validate"
        }
    }
    
}

public class AuthenticationNetwork: NetworkOperation<User?, AuthenticationApi>  {
    var network: Router<AuthenticationApi>
    
    public init(network: Router<AuthenticationApi>) {
        self.network = network
    }
    
    func login(username: String, password: String, token: String, onSuccess: ((User?) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .login(username: username, password: password, token: token)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
}

public class RegisterNetwork: NetworkOperation<ResponseError, AuthenticationApi> {
    var network: Router<AuthenticationApi>
    
    public init(network: Router<AuthenticationApi>) {
        self.network = network
    }
    
    func register(username: String, password: String, workerCode: String, onSuccess: ((ResponseError) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .register(username: username, password: password, workerCode: workerCode)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
}

public class RecordNetwork: NetworkOperation<ValidationResponse, AuthenticationApi> {
    var network: Router<AuthenticationApi>
    
    public init(network: Router<AuthenticationApi>) {
        self.network = network
    }
    
    func fetch(userId: String, token: String, onSuccess: ((ValidationResponse) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .fetch(userId: userId, token: token)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
}

