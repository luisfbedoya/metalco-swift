//
//  LocationValidation.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public enum LocationValidationApi {
    case detectFace(userId: String)
}

extension LocationValidationApi: EndPointType {
    public var path: String {
        switch self {
        case .detectFace: return "users/picture/detect"
        }
    }
    
    public var method: HTTPMethod {
        switch self {
        case .detectFace: return .post
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .detectFace(let userId): return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: ["Content-Type":"multipart/form-data; boundary=\(userId)"])
        }
    }
}

public class LocationValidationNetwork: NetworkOperation<ResponseError, LocationValidationApi> {
    var network: Router<LocationValidationApi>
    
    public init(network: Router<LocationValidationApi>) {
        self.network = network
    }

    func detectFace(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .detectFace(userId: userId)
        upload(network: network, with: data, userId: userId, type: "faceRecognition",onSuccess: { (response) in
            onSuccess?(response)
        }, onError: onError)
    }
}

