//
//  ProfileApi.swift
//  Metalco
//
//  Created by Felipe Bedoya on 05/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public enum ProfileApi {
    case profileImage(userId: String)
    case user(userId: String)
    case changePassword(userId: String, oldPassword: String, newPassword: String)
    case uploadPhoto(userId: String)

}
extension ProfileApi: EndPointType {
    public var path: String {
        switch self {
            case .profileImage: return "users/picture/byid"
            case .user: return "users/byid"
            case .changePassword: return "users/password"
            case .uploadPhoto: return "users/picture"
        }
    }
    
    public var method: HTTPMethod {
        switch self {
            case .profileImage: return .post
            case .user: return .post
            case .changePassword: return .post
            case .uploadPhoto: return .post
        }
    }
    
    public var task: HTTPTask {
        switch self {
            case .profileImage(let userId): return .requestParameters(bodyParameters: ["userID": userId], bodyEncoding: .jsonEncoding, urlParameters: nil)
            case .user(let userId): return .requestParameters(bodyParameters: ["userID": userId], bodyEncoding: .jsonEncoding, urlParameters: nil)
            case .changePassword(let userId, let oldPassword, let newPassword): return .requestParameters(bodyParameters: ["userID": userId, "password": oldPassword, "newPassword": newPassword], bodyEncoding: .jsonEncoding, urlParameters: nil)
            case .uploadPhoto(let userId): return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: ["Content-Type":"multipart/form-data; boundary=\(userId)"])
        }
    }
}

public class ProfileImageNetwork: NetworkOperation<Data, ProfileApi>  {
    var network: Router<ProfileApi>
    
    public init(network: Router<ProfileApi>) {
        self.network = network
    }
    
    func fetch(userId: String, onSuccess: ((Data) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .profileImage(userId: userId)
        executeRaw(network: network, onSuccess: { data in
            onSuccess?(data)
        }, onError: onError)
    }
}

public class ProfileNetwork: NetworkOperation<ProfileData, ProfileApi> {
    var network: Router<ProfileApi>
    
    public init(network: Router<ProfileApi>) {
        self.network = network
    }
    
    func fetch(userId: String, onSuccess: ((User) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .user(userId: userId)
        execute(network: network, onSuccess: { data in
            onSuccess?(data.data)
        }, onError: onError)
    }
}

public class ProfilePasswordNetwork: NetworkOperation<ResponseError, ProfileApi> {
    var network: Router<ProfileApi>
    
    public init(network: Router<ProfileApi>) {
        self.network = network
    }
    
    func changePassword(userId: String, oldPassword: String, newPassword: String, onSuccess: ((ResponseError) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .changePassword(userId: userId, oldPassword: oldPassword, newPassword: newPassword)
        execute(network: network, onSuccess: { data in
            onSuccess?(data)
        }, onError: onError)
    }
}

public class ProfileUploadPictureNetwork: NetworkOperation<ResponseError, ProfileApi> {
    var network: Router<ProfileApi>
    
    public init(network: Router<ProfileApi>) {
        self.network = network
    }
    
    func uploadPhoto(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .uploadPhoto(userId: userId)
        upload(network: network, with: data, userId: userId, type: "pictureProfile",onSuccess: { (response) in
            onSuccess?(response)
        }, onError: onError)
    }
}
