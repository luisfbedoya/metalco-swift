//
//  WarehouseApi.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public enum WarehouseApi {
    case warehouses
    case warehouseByWorker(workerId: String)
}

extension WarehouseApi: EndPointType {
    public var path: String {
        switch self {
        case .warehouseByWorker: return "warehouses/byworker"
        case .warehouses: return "warehouses"
        }
    }
    
    public var method: HTTPMethod {
        switch self {
        case .warehouses: return .get
        case .warehouseByWorker: return .post
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .warehouses: return .request
        case .warehouseByWorker(let workerId): return .requestParameters(bodyParameters: ["workerID": workerId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        }
    }
}

public class WarehousesNetwork: NetworkOperation<WarehousesData, WarehouseApi> {
    var network: Router<WarehouseApi>
    
    public init(network: Router<WarehouseApi>) {
        self.network = network
    }
    
    func fetch(onSuccess: (([Warehouse]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .warehouses
        execute(network: network, onSuccess: { (data) in
            if data.error == 0 {
                onSuccess?(data.data)
            } else {
                onError?(NetworkError.message("Error de conexión"))
            }
        }) { (err) in
            onError?(err)
        }
    }
}

public class WarehouseNetwork: NetworkOperation<WarehouseData, WarehouseApi> {
    var network: Router<WarehouseApi>
    
    public init(network: Router<WarehouseApi>) {
        self.network = network
    }
    
    func fetch(workerId: String, onSuccess: ((Warehouse?) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .warehouseByWorker(workerId: workerId)
        execute(network: network, onSuccess: { (data) in
            if data.error == 0 {
                onSuccess?(data.data)
            } else {
                onError?(NetworkError.message("Error de conexión"))
            }
        }) { (err) in
            onError?(err)
        }
    }
}
