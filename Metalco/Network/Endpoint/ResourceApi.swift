//
//  ResourceApi.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public enum ResourceApi {
    case worker(workerId: String)
    case warehouse(warehouseId: String)
    case workerTransfers(workerId: String)
    case warehouseTransfers(warehouseId: String)
    case acceptTransfer(transferId: String)
    case rejectTransfer(transferId: String)
    case cancelTransfer(transferId: String)
    case createTransfer(workerId: String, warehouseId: String, fromType: String, toType: String, toWorkerNumber: String, toWarehouseId: String, resourceId: String)
    case updateHistory(workerId: String, updateNumber: Int, money: Int, resourceId: String)
    case report(workerId: String, reason: String, resourceId: String)
    case maintenance(workerId: String, warehouseId: String, reason: String, resourceId: String)
    case finishMaintenance(resourceId: String)
    case cancelMaintenance(resourceId: String)
    case qr(qrId: String, workerId: String, latitude: Double, longitude: Double)
}

extension ResourceApi: EndPointType {
    public var path: String {
        switch self {
        case .worker: return "resources/byworker"
        case .warehouse: return "resources/bywarehouse"
        case .warehouseTransfers: return "transferresources/bywarehouse"
        case .workerTransfers: return "transferresources/byworker"
        case .acceptTransfer: return "transferresources/accept"
        case .rejectTransfer: return "transferresources/reject"
        case .cancelTransfer: return "transferresources/cancel"
        case .createTransfer: return "transferresources"
        case .updateHistory: return "resourcesupdatehistories"
        case .report: return "reportresources"
        case .maintenance: return "maintenanceresources"
        case .finishMaintenance: return "maintenanceresources/finish"
        case .cancelMaintenance: return "maintenanceresources/cancel"
        case .qr: return "resources/byqr"
        }
    }
    
    public var method: HTTPMethod {
        return .post
    }
    
    public var task: HTTPTask {
        switch self {
        case .worker(workerId: let workerId):
            return .requestParameters(bodyParameters: ["workerID": workerId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .warehouse(warehouseId: let warehouseId):
            return .requestParameters(bodyParameters: ["warehouseID": warehouseId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .warehouseTransfers(warehouseId: let warehouseId):
            return .requestParameters(bodyParameters: ["warehouseID": warehouseId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .workerTransfers(workerId: let workerId):
            return .requestParameters(bodyParameters: ["workerID": workerId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .acceptTransfer(transferId: let transferId):
            return .requestParameters(bodyParameters: ["transferID": transferId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .rejectTransfer(transferId: let transferId):
            return .requestParameters(bodyParameters: ["transferID": transferId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .cancelTransfer(transferId: let transferId):
            return .requestParameters(bodyParameters: ["transferID": transferId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .createTransfer(workerId: let workerId, warehouseId: let warehouseId, fromType: let fromType, toType: let toType, toWorkerNumber: let toWorkerNumber, toWarehouseId: let toWarehouseId, resourceId: let resourceId):
            return .requestParameters(bodyParameters: ["workerID": workerId, "warehouseID": warehouseId, "fromType": fromType, "toType": toType, "toWorkerNumber": toWorkerNumber, "toWarehouseID": toWarehouseId, "resourceID": resourceId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .updateHistory(workerId: let workerId, updateNumber: let updateNumber, money: let money, resourceId: let resourceId):
            return .requestParameters(bodyParameters: ["workerID": workerId, "updateNumber": updateNumber, "money": money, "resourceID": resourceId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .report(workerId: let workerId, reason: let reason, resourceId: let resourceId):
            return .requestParameters(bodyParameters: ["workerID": workerId, "reason": reason, "resourceID": resourceId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .maintenance(workerId: let workerId, warehouseId: let warehouseId, reason: let reason, resourceId: let resourceId):
            return .requestParameters(bodyParameters: ["workerID": workerId, "warehouseID": warehouseId, "reason": reason, "resourceID": resourceId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .finishMaintenance(resourceId: let resourceId):
            return .requestParameters(bodyParameters: ["resourceID": resourceId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .cancelMaintenance(resourceId: let resourceId):
            return .requestParameters(bodyParameters: ["resourceID": resourceId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .qr(qrId: let qrId, workerId: let workerId, latitude: let latitude, longitude: let longitude):
            return .requestParameters(bodyParameters: ["qrID": qrId, "workerID": workerId, "latitude": latitude, "longitude": longitude], bodyEncoding: .jsonEncoding, urlParameters: nil)
        }
    }
    
}

public class ResourcesNetwork: NetworkOperation<ResourcesData, ResourceApi> {
    var network: Router<ResourceApi>
    
    public init(network: Router<ResourceApi>) {
        self.network = network
    }
    
    func fetch(warehouse: String, onSuccess: (([Resource]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .warehouse(warehouseId: warehouse)
        execute(network: network, onSuccess: { (data) in
            if data.error == 0 {
                onSuccess?(data.data)
            } else {
                onError?(NetworkError.message("Error de conexión"))
            }
        }) { (err) in
            onError?(err)
        }
    }
    
    func fetch(workerId: String, onSuccess: (([Resource]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .worker(workerId: workerId)
        execute(network: network, onSuccess: { (data) in
            if data.error == 0 {
                onSuccess?(data.data)
            } else {
                onError?(NetworkError.message("Error de conexión"))
            }
        }) { (err) in
            onError?(err)
        }
    }
}

public class ResourceNetwork: NetworkOperation<ResourceData, ResourceApi> {
    var network: Router<ResourceApi>
    
    public init(network: Router<ResourceApi>) {
        self.network = network
    }
    
    func findById(resourceId: String, workerId: String, latitude: Double, longitude: Double, onSuccess: (([Resource]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .qr(qrId: resourceId, workerId: workerId, latitude: latitude, longitude: longitude)
        execute(network: network, onSuccess: { (data) in
            if data.error == 0 {
                onSuccess?([data.data])
            } else {
                onError?(NetworkError.message("No se encontró el recurso."))
            }
        }) { (err) in
            onError?(err)
        }
    }
}

public class ResourceActionNetwork: NetworkOperation<ResponseError, ResourceApi> {
    var network: Router<ResourceApi>

    public init(network: Router<ResourceApi>) {
        self.network = network
    }
    
    public func report(workerId: String, resourceId: String, reason: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .report(workerId: workerId, reason: reason, resourceId: resourceId)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
    
    public func transfer(workerId: String, warehouseId: String, resourceId: String, fromType: String, toType: String, toWorkerId: String, toWarehouseId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .createTransfer(workerId: workerId, warehouseId: warehouseId, fromType: fromType, toType: toType, toWorkerNumber: toWorkerId, toWarehouseId: toWarehouseId, resourceId: resourceId)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
    
    public func maintenance(workerId: String, warehouseId: String, resourceId: String, reason: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .maintenance(workerId: workerId, warehouseId: warehouseId, reason: reason, resourceId: resourceId)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
    
    public func update(workerId: String, resourceId: String, money: Int, updatedValue: Double, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .updateHistory(workerId: workerId, updateNumber: Int(updatedValue), money: money, resourceId: resourceId)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
    
    public func cancelMaintenance(resourceId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?){
        request = .cancelMaintenance(resourceId: resourceId)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
    
    public func finishMaintenance(resourceId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        request = .finishMaintenance(resourceId: resourceId)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
}
