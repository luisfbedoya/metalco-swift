//
//  DashboardApi.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public enum DashboardApi {
    case fetch(userId: String, workerId: String, recordId: String)
    case create(userId: String, workerId: String, workplaceId: String)
    case finish(userId: String, workerId: String, recordId: String)
    case checkInBreak(userId: String, workerId: String, recordId: String)
    case checkOutBreak(userId: String, workerId: String, recordId: String)
    case validateFace(userId: String)
    case validateAnonymousUser(username: String)
    case uploadFace(userId: String)
    case invalidate(userId: String, workerId: String, recordId: String)
}

extension DashboardApi: EndPointType {
    public var path: String {
        switch self {
        case .fetch:
            return "records/byid"
        case .create:
            return "records"
        case .finish:
            return "records"
        case .checkInBreak:
            return "records/inbreak"
        case .checkOutBreak:
            return "records/outbreak"
        case .validateFace:
            return "users/picture/detect"
        case .validateAnonymousUser:
            return "users/byusername"
        case .uploadFace:
            return "users/picture/uploadface"
        case .invalidate:
            return "records/invalidate"
        }
    }
    
    public var method: HTTPMethod {
        switch self {
        case .fetch:
            return .post
        case .create:
            return .post
        case .finish:
            return .put
        case .checkInBreak:
            return .post
        case .checkOutBreak:
            return .post
        case .validateFace:
            return .post
        case .validateAnonymousUser:
            return .post
        case .uploadFace:
            return .post
        case .invalidate:
            return .put
        }
    }
    
    public var task: HTTPTask {
        switch self {
        case .fetch(let userId, let workerId, let recordId): return .requestParameters(bodyParameters: ["userID": userId, "workerID": workerId, "recordID": recordId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .create(let userId, let workerCode, let workplaceId): return .requestParameters(bodyParameters: ["userID": userId, "workerID": workerCode, "workPlaceID": workplaceId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .finish(let userId, let workerCode, let recordId): return .requestParameters(bodyParameters: ["userID": userId, "workerID": workerCode, "recordID": recordId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .checkInBreak(let userId, let workerCode, let recordId): return .requestParameters(bodyParameters: ["userID": userId, "workerID": workerCode, "recordID": recordId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .checkOutBreak(let userId,let workerCode, let recordId): return .requestParameters(bodyParameters: ["userID": userId, "workerID": workerCode, "recordID": recordId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .validateFace(let userId): return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: ["Content-Type":"multipart/form-data; boundary=\(userId)"])
        case .validateAnonymousUser(let username): return .requestParameters(bodyParameters: ["username": username], bodyEncoding: .jsonEncoding, urlParameters: nil)
        case .uploadFace(let userId): return .requestParametersAndHeaders(bodyParameters: nil, bodyEncoding: .urlEncoding, urlParameters: nil, additionHeaders: ["Content-Type":"multipart/form-data; boundary=\(userId)"])
        case .invalidate(let userId, let workerCode, let recordId): return .requestParameters(bodyParameters: ["userID": userId, "workerID": workerCode, "recordID": recordId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        }
    }
}

public class DashboardNetwork: NetworkOperation<DashboardData, DashboardApi> {
    var network: Router<DashboardApi>
    
    public init(network: Router<DashboardApi>) {
        self.network = network
    }
    
    func fetchRecord(userId: String, workerId: String, recordId: String, onSuccess: ((Record) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .fetch(userId: userId, workerId: workerId, recordId: recordId)
        execute(network: network, onSuccess: { (response) in
            if let record = response.data {
                onSuccess?(record)
            } else {
                onError?(NetworkError.message("Error de servidor"))
            }
        }, onError: onError)
    }
    
    func createRecord(userId: String, workerId: String, workplaceId: String, onSuccess: ((Record) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .create(userId: userId, workerId: workerId, workplaceId: workplaceId)
        execute(network: network, onSuccess: { (response) in
            switch response.error {
            case 0:
                if let record = response.data {
                    onSuccess?(record)
                }
            case 2: onError?(NetworkError.message("Número de empleado no existe"))
            case 3: onError?(NetworkError.message("Este empleado no se encuentra asignado a esta ubicación"))
            default:
                onError?(NetworkError.message("Error de servidor"))
            }
        }, onError: onError)
    }
    
    func checkInBreak(userId: String, workerId: String, recordId: String, onSuccess: ((Record) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .checkInBreak(userId: userId, workerId: workerId, recordId: recordId)
        execute(network: network, onSuccess: { (response) in
            if let record = response.data {
                onSuccess?(record)
            } else {
                onError?(NetworkError.message("Error de servidor"))
            }
        }, onError: onError)
        
    }
    
    func checkOutBreak(userId: String, workerId: String, recordId: String, onSuccess: ((Record) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .checkOutBreak(userId: userId, workerId: workerId, recordId: recordId)
        execute(network: network, onSuccess: { (response) in
            if let record = response.data {
                onSuccess?(record)
            } else {
                onError?(NetworkError.message("Error de servidor"))
            }

        }, onError: onError)
    }
    
    
}

public class DashboardFinishRecordNetwork: NetworkOperation<ResponseError, DashboardApi> {
    var network: Router<DashboardApi>
    
    public init(network: Router<DashboardApi>) {
        self.network = network
    }

    func finishRecord(userId: String, workerId: String, recordId: String, onSuccess: ((ResponseError) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .finish(userId: userId, workerId: workerId, recordId: recordId)
        execute(network: network, onSuccess: { (response) in
            onSuccess?(response)
        }, onError: onError)
    }
    
    func invalidateRecord(userId: String, workerId: String, recordId: String, onSuccess: ((ResponseError) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .invalidate(userId: userId, workerId: workerId, recordId: recordId)
        execute(network: network, onSuccess: { (response) in
            onSuccess?(response)
        }, onError: onError)
    }
}


public class DashboardFaceRecognitionNetwork: NetworkOperation<ResponseError, DashboardApi> {
    var network: Router<DashboardApi>
    
    public init(network: Router<DashboardApi>) {
        self.network = network
    }

    func detectFace(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .validateFace(userId: userId)
        upload(network: network, with: data, userId: userId, type: "faceRecognition", onSuccess: { (response) in
            onSuccess?(response)
        }, onError: onError)
    }
}

public class DashboardUserNetwork: NetworkOperation<ProfileData, DashboardApi> {
    var network: Router<DashboardApi>
    
    public init(network: Router<DashboardApi>) {
        self.network = network
    }

    func fetchUser(username: String, onSuccess: ((ProfileData) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .validateAnonymousUser(username: username)
        execute(network: network, onSuccess: onSuccess, onError: onError)
    }
}

public class DashboardUploadFaceNetwork: NetworkOperation<ResponseError, DashboardApi> {
    var network: Router<DashboardApi>
    
    public init(network: Router<DashboardApi>) {
        self.network = network
    }

    func uploadPhoto(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .uploadFace(userId: userId)
        upload(network: network, with: data, userId: userId, type: "historyFace", onSuccess: onSuccess, onError: onError)
    }
}
