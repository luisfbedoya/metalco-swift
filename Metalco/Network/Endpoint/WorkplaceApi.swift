//
//  WorkplaceEndPoint.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public enum WorkplaceApi {
    case fetch(userId: String, workerId: String)
    case validateManager(workerId: String)

}

extension WorkplaceApi: EndPointType {
    public var path: String {
        switch self {
            case .fetch: return "workplaces/byuser"
            case .validateManager: return "workers/hasmanager"
        }
    }
    
    public var method: HTTPMethod {
        switch self {
            case .fetch: return .post
            case .validateManager: return .post
        }
    }
    
    public var task: HTTPTask {
        switch self {
            case .fetch(let userId, let workerId): return .requestParameters(bodyParameters: ["userID": userId, "workerID": workerId], bodyEncoding: .jsonEncoding, urlParameters: nil)
            case .validateManager(let workerId): return .requestParameters(bodyParameters: [ "workerID": workerId], bodyEncoding: .jsonEncoding, urlParameters: nil)
        }
    }
}

public class WorkplaceNetwork: NetworkOperation<WorkplaceData, WorkplaceApi>  {
    var network: Router<WorkplaceApi>
    
    public init(network: Router<WorkplaceApi>) {
        self.network = network
    }
    
    func fetch(userId: String, workerId:String, onSuccess: (([Workplace]) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .fetch(userId: userId, workerId: workerId)
        execute(network: network, onSuccess: { (data) in
            onSuccess?(data.data)
        }, onError: onError)
    }
}

public class WorkplaceValidationNetwork: NetworkOperation<ResponseError, WorkplaceApi>  {
    var network: Router<WorkplaceApi>
    
    public init(network: Router<WorkplaceApi>) {
        self.network = network
    }
    
    func validateManager(workerId:String, onSuccess: ((ResponseError) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        request = .validateManager(workerId: workerId)
        execute(network: network, onSuccess: { (data) in
            onSuccess?(data)
        }, onError: onError)
    }
}
