//
//  EndPointType.swift
//  Metalco
//
//  Created by Felipe Bedoya on 30/04/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public protocol EndPointType {
    var path: String {get}
    var method: HTTPMethod {get}
    var task: HTTPTask {get}
}
