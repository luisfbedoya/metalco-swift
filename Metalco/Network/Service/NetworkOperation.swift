//
//  NetworkOperation.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

enum NetworkResponse:String {
    case success
    case authenticationError = "Error de usuario y contraseña."
    case badRequest = "Bad request"
    case outdated = "The url you requested is outdated."
    case failed = "Network request failed."
    case noData = "Response returned with no data to decode."
    case unableToDecode = "We could not decode the response."
}

enum Result<String>{
    case success
    case failure(String)
}


open class NetworkOperation<Output: Decodable, Endpoint: EndPointType> {
    typealias T = Output
    
    var request: Endpoint?
    public init() { }

    
    func execute(network: Router<Endpoint>, onSuccess: ((Output) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        guard let request = self.request else {
            fail(error: NetworkError.missingPath, to: onError)
            return
        }
        network.execute(request) { (data, response, error) in
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                switch result {
                case .success:
                    guard let responseData = data else {
                        return
                    }
                    do {
                        let apiResponse = self.decode(from: responseData)
                        guard let json = apiResponse else {
                            throw NetworkError.dataIsNotEncodable("")
                        }
                        self.dispatch(json: json, to: onSuccess)
                    }catch {
                        self.fail(error: .message("Error de servidor"), to: onError)
                    }
                case .failure(let message):
                    self.fail(error: .message(message), to: onError)
                }
            } else {
                self.fail(error: .message("Error de red. Verifique su conexión a internet."), to: onError)
            }
        }
    }
    
    func executeRaw(network: Router<Endpoint>, onSuccess: ((Data) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        guard let request = self.request else {
            fail(error: NetworkError.missingPath, to: onError)
            return
        }
        network.execute(request) { (data, response, error) in
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                
                switch result {
                case .success:
                    guard let responseData = data else {
                        return
                    }
                    print(responseData)
                    self.dispatch(data: responseData, to: onSuccess)
                case .failure( _):
                    self.fail(error: .message("Network failure"), to: onError)
                }
            } else {
                self.fail(error: .message("Error de red. Verifique su conexión a internet."), to: onError)
            }
        }
    }
    
    func upload(network: Router<Endpoint>, with data: Data, userId: String, type: String, onSuccess: ((Output) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        guard let request = self.request else {
            fail(error: NetworkError.missingPath, to: onError)
            return
        }
        network.upload(to: request, data: data, userId: userId, type: type) { (data, response, error) in
            if let response = response as? HTTPURLResponse {
                let result = self.handleNetworkResponse(response)
                
                switch result {
                case .success:
                    guard let responseData = data else {
                        return
                    }
                    do {
                        print(responseData)
                        let apiResponse = self.decode(from: responseData)
                        guard let json = apiResponse else {
                            throw NetworkError.dataIsNotEncodable("")
                        }
                        self.dispatch(json: json, to: onSuccess)
                    }catch {
                        print(self.decodeRaw(from: responseData))
                        self.fail(error: .dataIsNotEncodable("Failed to decode"), to: onError)
                    }
                case .failure( _):
                    self.fail(error: .message("Network failure"), to: onError)
                }
            } else {
                self.fail(error: .message("Error de red. Verifique su conexión a internet."), to: onError)
            }
        }
    }
    
    func decode(from data: Data) -> Output? {
        do {
            return try JSONDecoder().decode(Output.self, from: data)
        } catch {
            return nil
        }
    }
    
    func decodeRaw(from data: Data) -> String {
        return String(decoding: data, as: UTF8.self)
    }
    
    func dispatch(json: T, to onSuccess: ((T) -> Void)?) {
        DispatchQueue.main.async {
            onSuccess?(json)
        }
    }
    
    func dispatch(data: Data, to onSuccess: ((Data) -> Void)?) {
        DispatchQueue.main.async {
            onSuccess?(data)
        }
    }
    
    func fail(error: NetworkError, to onError: ((NetworkError) -> Void)?) {
        DispatchQueue.main.async {
            onError?(error)
        }
    }
    
    fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String>{
        switch response.statusCode {
            case 200...299: return .success
            case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
            case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
            case 600: return .failure(NetworkResponse.outdated.rawValue)
            default: return .failure(NetworkResponse.failed.rawValue)
        }
    }
}
