//
//  HTTPMethod.swift
//  Metalco
//
//  Created by Felipe Bedoya on 30/04/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
