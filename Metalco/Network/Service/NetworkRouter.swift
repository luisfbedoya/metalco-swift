//
//  NetworkRouter.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import UIKit

public typealias NetworkRouterCompletion = (_ data: Data?,_ response: URLResponse?,_ error: Error?)->()

public protocol NetworkRouter: class {
    associatedtype EndPoint: EndPointType
    func execute(_ route: EndPoint, completion: @escaping NetworkRouterCompletion)
    func cancel()
}

public class Router<EndPoint: EndPointType>: NetworkRouter {
    #if DEBUG
        private var baseURL = URL(string: "http://192.168.0.6:3005/api")!
    #else
        private var baseURL = URL(string: "http://199.195.116.169:3005/api")!
    #endif
    private var task: URLSessionTask?
    
    public func execute(_ route: EndPoint, completion: @escaping NetworkRouterCompletion) {
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 40
        do {
            let request = try self.buildRequest(from: route)
            NetworkLogger.log(request: request)
            task = session.dataTask(with: request, completionHandler: { data, response, error in
              completion(data, response, error)
            })
        }catch {
            completion(nil, nil, error)
        }
        self.task?.resume()
    }
    
    public func upload(to route: EndPoint, data: Data, userId: String, type: String, completion: @escaping NetworkRouterCompletion) {
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 40
        do {
            let request = try self.buildRequest(from: route)
            let requestData = self.build(data: data, userId: userId, type: type)
            NetworkLogger.log(request: request)
            task = session.uploadTask(with: request, from: requestData, completionHandler: { (data, response, error) in
                completion(data, response, error)
            })
        }catch {
            completion(nil, nil, error)
        }
        self.task?.resume()
    }
    
    
    public func cancel() {
        self.task?.cancel()
    }
    
    func build(data: Data, userId: String, type: String) -> Data{
        let filename = userId

        // generate boundary string using a unique per-app string
        let boundary = userId

        let fieldName = "reqtype"
        let fieldValue = "fileupload"

        let fieldName2 = "userID"
        let fieldValue2 = userId
        
        let fieldName3 = "type"
        let fieldValue3 = type


        var requestData = Data()

        // Add the reqtype field and its value to the raw http request data
        requestData.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        requestData.append("Content-Disposition: form-data; name=\"\(fieldName)\"\r\n\r\n".data(using: .utf8)!)
        requestData.append("\(fieldValue)".data(using: .utf8)!)

        // Add the userhash field and its value to the raw http reqyest data
        requestData.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        requestData.append("Content-Disposition: form-data; name=\"\(fieldName2)\"\r\n\r\n".data(using: .utf8)!)
        requestData.append("\(fieldValue2)".data(using: .utf8)!)
        
        
        // Add the userhash field and its value to the raw http reqyest data
        requestData.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        requestData.append("Content-Disposition: form-data; name=\"\(fieldName3)\"\r\n\r\n".data(using: .utf8)!)
        requestData.append("\(fieldValue3)".data(using: .utf8)!)
        
        // Add the image data to the raw http request data
        requestData.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        requestData.append("Content-Disposition: form-data; name=\"picture\"; filename=\"\(filename)\"\r\n".data(using: .utf8)!)
        requestData.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        requestData.append(data)

        // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
        // According to the HTTP 1.1 specification https://tools.ietf.org/html/rfc7230
        requestData.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        return requestData
    }
    
    func buildRequest(from route: EndPoint) throws -> URLRequest {
        var request = URLRequest(url: baseURL.appendingPathComponent(route.path),
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 10.0)
        
        request.httpMethod = route.method.rawValue
        do {
            switch route.task {
            case .request:
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            case .requestParameters(let bodyParameters,
                                    let bodyEncoding,
                                    let urlParameters):
                
                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
                
            case .requestParametersAndHeaders(let bodyParameters,
                                              let bodyEncoding,
                                              let urlParameters,
                                              let additionalHeaders):
                
                self.addAdditionalHeaders(additionalHeaders, request: &request)
                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
            case .requestParametersStream(let bodyParameters,
                                          let bodyEncoding,
                                          let urlParameters):
                try self.configureParameters(bodyParameters: bodyParameters,
                                             bodyEncoding: bodyEncoding,
                                             urlParameters: urlParameters,
                                             request: &request)
            }
            return request
        } catch {
            throw error
        }
    }
    
    func configureParameters(bodyParameters: Parameters?,
                                         bodyEncoding: ParameterEncoding,
                                         urlParameters: Parameters?,
                                         request: inout URLRequest) throws {
        do {
            try bodyEncoding.encode(urlRequest: &request,
                                    bodyParameters: bodyParameters, urlParameters: urlParameters)
        } catch {
            throw error
        }
    }
    
    func addAdditionalHeaders(_ additionalHeaders: HTTPHeaders?, request: inout URLRequest) {
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
}
