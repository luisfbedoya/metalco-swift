//
//  NetworkHistoryUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public final class NetworkHistoryUseCase: HistoryUseCase {
    private let network: HistoryNetwork
    
    init(network: HistoryNetwork) {
        self.network = network
    }
    
    func fetch(userId: String, workerId: String, onSuccess: (([Record]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.fetch(userId: userId, workerId: workerId, onSuccess: onSuccess, onError: onError)
    }
}
