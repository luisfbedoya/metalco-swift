//
//  NetworkUseCaseProvider.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public final class NetworkUseCaseProvider: UseCaseProvider {
    private let networkProvider: NetworkProvider
    
    init() {
        networkProvider = NetworkProvider()
    }
    
    public func makeAuthenticationUseCase() -> AuthenticationUseCase {
        return NetworkAuthenticationUseCase(network: networkProvider.makeAuthenticationNetwork(), registerNetwork: networkProvider.makeRegisterNetwork(), recordNetwork: networkProvider.makeRecordNetwork())
    }
    
    func makeHistoryUseCase() -> HistoryUseCase {
        return NetworkHistoryUseCase(network: networkProvider.makeHistoryNetwork())
    }
    
    func makeProfileUseCase() -> ProfileUseCase {
        return NetworkProfileUseCase(network: networkProvider.makeProfileNetwork(), networkImage: networkProvider.makeProfileImageNetwork(), networkPassword: networkProvider.makeProfilePasswordNetwork(), networkUpload: networkProvider.makeProfileUploadNetwork())
    }
    
    func makeWorkplaceUseCase() -> WorkplaceUseCase {
        return NetworkWorkplaceUseCase(network: networkProvider.makeWorkplaceNetwork(), networkValidate: networkProvider.makeWorkplaceValidationNetwork())
    }
    
    func makeLocationValidationUseCase() -> LocationValidationUseCase {
        return NetworkLocationValidationUseCase(network: networkProvider.makeLocationValidationNetwork())
    }
    
    func makeDashboardUseCase() -> DashboardUseCase {
        return NetworkDashboardUseCase(network: networkProvider.makeDashboardNetwork(), finishNetwork: networkProvider.makeDashboardFinishRecordNetwork(), userNetwork: networkProvider.makeDashboardUserNetwork(), faceNetwork: networkProvider.makeDashboardFaceRecognitionNetwork(), uploadNetwork: networkProvider.makeDashboardUploadFaceNetwork())
    }
    
    func makeWarehouseUseCase() -> WarehouseUseCase {
        return NetworkWarehouseUseCase(network: networkProvider.makeWarehousesNetwork(), networkWorker: networkProvider.makeWarehouseNetwork())
    }
    
    func makeResourceUseCase() -> ResourceUseCase {
        return NetworkResourceUseCase(network: networkProvider.makeResourcesNetwork(), actionsNetwork: networkProvider.makeResourceActionNetwork(), singleNetwork: networkProvider.makeResourceNetwork())
    }
}
