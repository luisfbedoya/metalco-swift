//
//  NetworkDashboardUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public class NetworkDashboardUseCase: DashboardUseCase {
    
    let network: DashboardNetwork
    let userNetwork: DashboardUserNetwork
    let faceNetwork: DashboardFaceRecognitionNetwork
    let finishNetwork: DashboardFinishRecordNetwork
    let uploadNetwork: DashboardUploadFaceNetwork
    
    init(network: DashboardNetwork, finishNetwork: DashboardFinishRecordNetwork, userNetwork: DashboardUserNetwork, faceNetwork: DashboardFaceRecognitionNetwork, uploadNetwork: DashboardUploadFaceNetwork) {
        self.network = network
        self.finishNetwork = finishNetwork
        self.userNetwork = userNetwork
        self.faceNetwork = faceNetwork
        self.uploadNetwork = uploadNetwork
    }
    
    
    func fetchRecord(userId: String, workerId: String, recordId: String, onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.fetchRecord(userId: userId, workerId: workerId, recordId: recordId, onSuccess: onSuccess, onError: onError)
    }
    
    func createRecord(userId: String, workerId: String, workplaceId: String, onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.createRecord(userId: userId, workerId: workerId, workplaceId: workplaceId, onSuccess: onSuccess, onError: onError)
    }
    
    func finishRecord(userId: String, workerId: String, recordId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        finishNetwork.finishRecord(userId: userId, workerId: workerId, recordId: recordId, onSuccess: onSuccess, onError: onError)
    }
    
    func checkInBreak(userId: String, workerId: String, recordId: String, onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.checkInBreak(userId: userId, workerId: workerId, recordId: recordId, onSuccess: onSuccess, onError: onError)
    }
    
    func checkOutBreak(userId: String, workerId: String, recordId: String, onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.checkOutBreak(userId: userId, workerId: workerId, recordId: recordId, onSuccess: onSuccess, onError: onError)
    }
    
    func createAnonymousRecord(userId: String, workerId: String, workplaceId: String, onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.createRecord(userId: userId, workerId: workerId, workplaceId: workplaceId, onSuccess: onSuccess, onError: onError)
    }
    
    func finishAnonymousRecord(userId: String, workerId: String, recordId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        finishNetwork.finishRecord(userId: userId, workerId: workerId, recordId: recordId, onSuccess: onSuccess, onError: onError)
    }
    
    func validateFace(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        faceNetwork.detectFace(userId: userId, data: data, onSuccess: onSuccess, onError: onError)
    }
    
    func validateAnonymousUser(username: String, onSuccess: ((ProfileData) -> Void)?, onError: ((NetworkError) -> Void)?) {
        userNetwork.fetchUser(username: username, onSuccess: onSuccess) { (error) in
            if error.localizedDescription.contains("Error de red") {
                onError?(NetworkError.message(error.localizedDescription))
            } else {
                onError?(NetworkError.message("El nombre de usuario no existe."))
            }
        }
    }
    
    func uploadFace(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        uploadNetwork.uploadPhoto(userId: userId, data: data, onSuccess: onSuccess, onError: onError)
    }
    
    func invalidate(userId: String, workerId: String, recordId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        finishNetwork.invalidateRecord(userId: userId, workerId: workerId, recordId: recordId, onSuccess: onSuccess, onError: onError)
    }
}
