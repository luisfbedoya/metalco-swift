//
//  AuthenticationUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public final class NetworkAuthenticationUseCase: AuthenticationUseCase {
    
    private let network: AuthenticationNetwork
    private let registerNetwork: RegisterNetwork
    private let recordNetwork: RecordNetwork
    
    init(network: AuthenticationNetwork, registerNetwork: RegisterNetwork, recordNetwork: RecordNetwork) {
        self.network = network
        self.registerNetwork = registerNetwork
        self.recordNetwork = recordNetwork
    }
    
    public func login(username: String, password: String, token: String, onSuccess: ((User?) -> Void)? = nil, onError: ((NetworkError) -> Void)? = nil) {
        network.login(username: username, password: password, token: token, onSuccess: onSuccess, onError: onError)
    }
    
    public func register(username: String, password: String, workerCode: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        registerNetwork.register(username: username, password: password, workerCode: workerCode, onSuccess: { (response) in
            switch response.error {
                case 0: onSuccess?(response)
                case 1: onError?(NetworkError.message("Error de servidor"))
                case 2: onError?(NetworkError.message("Nombre de usuario ya existe"))
                case 3: onError?(NetworkError.message("Número de empleado no existe"))
            default:
                onError?(NetworkError.message("Error de servidor"))
            }
        }, onError: onError)
    }
    
    public func fetch(userId: String, token: String, onSuccess: ((ValidationResponse) -> Void)?, onError: ((NetworkError) -> Void)?) {
        recordNetwork.fetch(userId: userId, token: token, onSuccess: onSuccess, onError: onError)
    }
}
