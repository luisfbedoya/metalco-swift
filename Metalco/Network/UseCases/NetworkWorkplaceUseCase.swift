//
//  NetworkWorkplaceUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public final class NetworkWorkplaceUseCase: WorkplaceUseCase {
    let network: WorkplaceNetwork
    let networkValidate: WorkplaceValidationNetwork
    init(network: WorkplaceNetwork, networkValidate: WorkplaceValidationNetwork) {
        self.network = network
        self.networkValidate = networkValidate
    }
    
    func fetch(userId: String, workerId: String, onSuccess: (([Workplace]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.fetch(userId: userId, workerId: workerId, onSuccess: onSuccess, onError: onError)
    }
    
    func validateManager(workerId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        networkValidate.validateManager(workerId: workerId, onSuccess: onSuccess, onError: onError)
    }
}
