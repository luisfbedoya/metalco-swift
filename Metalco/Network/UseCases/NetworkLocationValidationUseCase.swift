//
//  NetworkLocationValidationUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public class NetworkLocationValidationUseCase: LocationValidationUseCase {
    
    let network: LocationValidationNetwork
    
    init(network: LocationValidationNetwork) {
        self.network = network
    }
    
    func detectFace(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.detectFace(userId: userId, data: data, onSuccess: onSuccess, onError: onError)
    }
}
