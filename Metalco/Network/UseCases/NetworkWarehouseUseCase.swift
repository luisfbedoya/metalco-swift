//
//  NetworkWarehouseUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public final class NetworkWarehouseUseCase: WarehouseUseCase {
    private let network: WarehousesNetwork
    private let networkWorker: WarehouseNetwork
    
    init(network: WarehousesNetwork, networkWorker: WarehouseNetwork) {
        self.network = network
        self.networkWorker = networkWorker
    }
    
    func fetch(onSuccess: (([Warehouse]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.fetch(onSuccess: onSuccess, onError: onError)
    }
    
    func warehouses(workerId: String, onSuccess: ((Warehouse?) -> Void)?, onError: ((NetworkError) -> Void)?) {
        networkWorker.fetch(workerId: workerId, onSuccess: onSuccess, onError: onError)
    }
    
    func resourcesByWarehouse(warehouseId: String, onSuccess: ((Warehouse?) -> Void)?, onError: ((NetworkError) -> Void)?) {
        
    }
}
