//
//  NetworkProfileUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 05/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public final class NetworkProfileUseCase: ProfileUseCase {
    let network: ProfileNetwork
    let networkImage: ProfileImageNetwork
    let networkPassword: ProfilePasswordNetwork
    let networkUpload: ProfileUploadPictureNetwork
    
    init(network: ProfileNetwork, networkImage: ProfileImageNetwork, networkPassword: ProfilePasswordNetwork, networkUpload: ProfileUploadPictureNetwork) {
        self.network = network
        self.networkImage = networkImage
        self.networkPassword = networkPassword
        self.networkUpload = networkUpload
    }
    
    func fetch(userId: String, onSuccess: ((Data) -> Void)?, onError: ((NetworkError) -> Void)?) {
        network.fetch(userId: userId, onSuccess: { (user) in
            UserService.shared.user = user
            self.networkImage.fetch(userId: userId, onSuccess: onSuccess, onError: onError)
        }, onError: onError)
    }
    
    func changePassword(userId: String, currentPassword: String, newPassword: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        networkPassword.changePassword(userId: userId, oldPassword: currentPassword, newPassword: newPassword, onSuccess: onSuccess, onError: onError)
    }
    
    func uploadPhoto(userId: String, photo: Data, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        networkUpload.uploadPhoto(userId: userId, data: photo, onSuccess: onSuccess, onError: onError)
    }
    

}
