//
//  NetworkResourceUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 13/12/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public final class NetworkResourceUseCase: ResourceUseCase {
    
    private let network: ResourcesNetwork
    private let actionsNetwork: ResourceActionNetwork
    private let singleNetwork: ResourceNetwork

    init(network: ResourcesNetwork, actionsNetwork: ResourceActionNetwork, singleNetwork: ResourceNetwork) {
        self.network = network
        self.actionsNetwork = actionsNetwork
        self.singleNetwork = singleNetwork
    }
    
    public func resourcesByWarehouse(warehouseId: String, onSuccess: (([Resource]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        self.network.fetch(warehouse: warehouseId, onSuccess: onSuccess, onError: onError)
    }
    
    public func resourcesByWorker(workerId: String, onSuccess: (([Resource]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        self.network.fetch(workerId: workerId, onSuccess: onSuccess, onError: onError)
    }
    
    public func report(workerId: String, resourceId: String, reason: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        actionsNetwork.report(workerId: workerId, resourceId: resourceId, reason: reason, onSuccess: onSuccess, onError: onError)
    }
    
    public func transfer(workerId: String, warehouseId: String, resourceId: String, fromType: String, toType: String, toWorkerId: String, toWarehouseId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        actionsNetwork.transfer(workerId: workerId, warehouseId: warehouseId, resourceId: resourceId, fromType: fromType, toType: toType, toWorkerId: toWorkerId, toWarehouseId: toWarehouseId, onSuccess: onSuccess, onError: onError)
    }
    
    public func update(workerId: String, resourceId: String, money: Int, updatedValue: Double, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        actionsNetwork.update(workerId: workerId, resourceId: resourceId, money: money, updatedValue: updatedValue, onSuccess: onSuccess, onError: onError)
    }
    
    public func maintenance(workerId: String, warehouseId: String, resourceId: String, reason: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        actionsNetwork.maintenance(workerId: workerId, warehouseId: warehouseId, resourceId: resourceId, reason: reason, onSuccess: onSuccess, onError: onError)
    }
    
    public func cancelMaintenance(resourceId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        actionsNetwork.cancelMaintenance(resourceId: resourceId, onSuccess: onSuccess, onError: onError)
    }
    
    public func finishMaintenance(resourceId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?) {
        actionsNetwork.finishMaintenance(resourceId: resourceId, onSuccess: onSuccess, onError: onError)
    }
    
    public func findById(resourceId: String, workerId: String, latitude: Double, longitude: Double, onSuccess: (([Resource]) -> Void)?, onError: ((NetworkError) -> Void)?) {
        singleNetwork.findById(resourceId: resourceId, workerId: workerId, latitude: latitude, longitude: longitude, onSuccess: onSuccess, onError: onError)
    }
}
