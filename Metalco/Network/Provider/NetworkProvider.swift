//
//  NetworkProvider.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public class NetworkProvider {
    
    func makeAuthenticationNetwork() -> AuthenticationNetwork {
        return AuthenticationNetwork(network: Router<AuthenticationApi>())
    }
    
    func makeRegisterNetwork() -> RegisterNetwork {
        return RegisterNetwork(network: Router<AuthenticationApi>())
    }
    
    func makeRecordNetwork() -> RecordNetwork {
        return RecordNetwork(network: Router<AuthenticationApi>())
    }
    
    func makeHistoryNetwork() -> HistoryNetwork {
        return HistoryNetwork(network: Router<HistoryApi>())
    }
    
    func makeProfileNetwork() -> ProfileNetwork {
        return ProfileNetwork(network: Router<ProfileApi>())
    }
    
    func makeProfileImageNetwork() -> ProfileImageNetwork {
        return ProfileImageNetwork(network: Router<ProfileApi>())
    }
    
    func makeProfilePasswordNetwork() -> ProfilePasswordNetwork {
        return ProfilePasswordNetwork(network: Router<ProfileApi>())
    }
    
    func makeProfileUploadNetwork() -> ProfileUploadPictureNetwork {
        return ProfileUploadPictureNetwork(network: Router<ProfileApi>())
    }
    
    func makeWorkplaceNetwork() -> WorkplaceNetwork {
        return WorkplaceNetwork(network: Router<WorkplaceApi>())
    }
    
    func makeWorkplaceValidationNetwork() -> WorkplaceValidationNetwork {
        return WorkplaceValidationNetwork(network: Router<WorkplaceApi>())
    }
    
    func makeLocationValidationNetwork() -> LocationValidationNetwork {
        return LocationValidationNetwork(network: Router<LocationValidationApi>())
    }
    
    func makeDashboardNetwork() -> DashboardNetwork {
        return DashboardNetwork(network: Router<DashboardApi>())
    }
    
    func makeDashboardFinishRecordNetwork() -> DashboardFinishRecordNetwork {
        return DashboardFinishRecordNetwork(network: Router<DashboardApi>())
    }
    
    func makeDashboardUserNetwork() -> DashboardUserNetwork {
        return DashboardUserNetwork(network: Router<DashboardApi>())
    }
    
    func makeDashboardFaceRecognitionNetwork() -> DashboardFaceRecognitionNetwork {
        return DashboardFaceRecognitionNetwork(network: Router<DashboardApi>())
    }
    
    func makeDashboardUploadFaceNetwork() -> DashboardUploadFaceNetwork {
        return DashboardUploadFaceNetwork(network: Router<DashboardApi>())
    }
    
    func makeWarehousesNetwork() -> WarehousesNetwork {
        return WarehousesNetwork(network: Router<WarehouseApi>())
    }
    
    func makeWarehouseNetwork() -> WarehouseNetwork {
        return WarehouseNetwork(network: Router<WarehouseApi>())
    }
    
    func makeResourcesNetwork() -> ResourcesNetwork {
        return ResourcesNetwork(network: Router<ResourceApi>())
    }
    
    func makeResourceNetwork() -> ResourceNetwork {
        return ResourceNetwork(network: Router<ResourceApi>())
    }
    
    func makeResourceActionNetwork() -> ResourceActionNetwork {
        return ResourceActionNetwork(network: Router<ResourceApi>())
    }
}
