//
//  ResponseError.swift
//  Metalco
//
//  Created by Felipe Bedoya on 10/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public struct ResponseError: Codable {
    public let error: Int
}
