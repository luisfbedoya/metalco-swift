//
//  DashboardData.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public struct DashboardData: Codable {
    public let error: Int
    public let data: Record?
}
