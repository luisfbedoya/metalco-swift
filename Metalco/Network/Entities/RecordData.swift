//
//  RecordData.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public struct RecordData: Codable {
    public let data: [Record]
}
