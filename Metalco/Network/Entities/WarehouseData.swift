//
//  WarehouseData.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public struct WarehousesData: Codable {
    public let error: Int? = 0
    public let data: [Warehouse]
}

public struct WarehouseData: Codable {
    public let error: Int? = 0
    public let data: Warehouse?
}


