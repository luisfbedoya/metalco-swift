//
//  WorkplaceData.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public struct WorkplaceData: Codable {
    public let data: [Workplace]
}
