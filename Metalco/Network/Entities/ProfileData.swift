//
//  ProfileData.swift
//  Metalco
//
//  Created by Felipe Bedoya on 07/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public struct ProfileData: Codable {
    public let error: Int
    public let data: User
}
