//
//  ValidationResponse.swift
//  Metalco
//
//  Created by Felipe Bedoya on 14/07/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public struct ValidationResponse: Codable {
    public let error: Int
    public let data: ValidationData?
}

public struct ValidationData: Codable {
    public let user: User?
    public let record: Record?
    public let validate: Bool
}
