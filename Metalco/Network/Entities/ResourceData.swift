//
//  ResourceData.swift
//  Metalco
//
//  Created by Felipe Bedoya on 13/12/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public struct ResourcesData: Codable {
    public let error: Int? = 0
    public let data: [Resource]
}

public struct ResourceData: Codable {
    public let error: Int? = 0
    public let data: Resource
}
