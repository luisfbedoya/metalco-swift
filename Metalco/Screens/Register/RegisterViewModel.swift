//
//  RegisterViewModel.swift
//  Metalco
//
//  Created by Felipe Bedoya on 22/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol RegisterDelegate {
    func onSuccess()
    func onError(error: NetworkError)
}

public class RegisterViewModel {
    
    private let useCase: AuthenticationUseCase
    var delegate: RegisterDelegate? = nil
    
    init(useCase: AuthenticationUseCase) {
        self.useCase = useCase
    }
    
    func register(username: String, password: String, workerCode: String) {
        useCase.register(username: username, password: password, workerCode: workerCode, onSuccess: { (response) in
            self.delegate?.onSuccess()
        }) { (error) in
            self.delegate?.onError(error: error)
        }
    }
}
