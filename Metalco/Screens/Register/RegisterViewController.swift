//
//  RegisterViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 22/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class RegisterViewController: UIViewController {
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var workerNumberTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var repeatPasswordTF: UITextField!
    var viewModel: RegisterViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
    }
    
    @IBAction func backClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didClickReturn(_ sender: UITextField) {
        switch sender {
        case usernameTF:
            self.workerNumberTF.becomeFirstResponder()
        case workerNumberTF:
            self.passwordTF.becomeFirstResponder()
        case passwordTF:
            self.repeatPasswordTF.becomeFirstResponder()
        case repeatPasswordTF:
            self.registerClick(sender)
            self.view.endEditing(true)
        default:
            break
        }
    }
    
    @IBAction func registerClick(_ sender: Any) {
        usernameTF.borderColor = .lightGray
        workerNumberTF.borderColor = .lightGray
        passwordTF.borderColor = .lightGray
        repeatPasswordTF.borderColor = .lightGray
        if usernameTF.getText().isEmpty {
            showMessage(title: "Error", message: "Nombre de usuario vacio")
            usernameTF.borderColor = .red
            return
        }
        if workerNumberTF.getText().isEmpty {
            showMessage(title: "Error", message: "Número de empleado vacio")
            workerNumberTF.borderColor = .red
            return
        }
        if passwordTF.getText().isEmpty {
            showMessage(title: "Error", message: "Contraseña vacio")
            passwordTF.borderColor = .red
            return
        }
        if repeatPasswordTF.getText().isEmpty {
            showMessage(title: "Error", message: "Repetir contraseña vacio")
            repeatPasswordTF.borderColor = .red
            return
        }
        
        if !passwordTF.getText().elementsEqual(repeatPasswordTF.getText()) {
            showMessage(title: "Error", message: "Contraseñas no coinciden")
            passwordTF.borderColor = .red
            repeatPasswordTF.borderColor = .red
            return
        }
        showAnimation()
        viewModel.register(username: usernameTF.getText(), password: passwordTF.getText(), workerCode: workerNumberTF.getText())
    }
}

extension RegisterViewController: RegisterDelegate {
    func onSuccess() {
        stopAnimation()
        showMessage(title: "Exito", message: "Usuario registrado") { (action) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func onError(error: NetworkError) {
        stopAnimation()
        showMessage(title: "Error", message: error.localizedDescription)
    }
}
