//
//  NotAuthorizedViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 14/07/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

protocol NotAuthorizedDelegate {
    func logout()
    func refresh()
}

class NotAuthorizedViewController: UIViewController {
    var delegate: NotAuthorizedDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func logoutClick(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.logout()
        }
    }
    
    @IBAction func refreshClick(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.refresh()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
