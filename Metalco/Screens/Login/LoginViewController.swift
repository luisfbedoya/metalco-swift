//
//  LoginViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 30/04/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    var viewModel: LoginViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        showAnimation()
        viewModel.validate()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func loginClickButton(_ sender: Any) {
        let username = usernameTF.getText()
        let password = passwordTF.getText()
        showAnimation()
        self.viewModel.login(username: username, password: password)
    }
    
    @IBAction func didClickReturn(_ sender: UITextField) {
        switch sender {
        case usernameTF:
            self.passwordTF.becomeFirstResponder()
        case passwordTF:
            self.loginClickButton(sender)
            self.view.endEditing(true)
        default:
            break
        }
    }

    @IBAction func registerClick(_ sender: Any) {
        viewModel.toRegister()
    }
    
}

extension LoginViewController: LoginDelegate {
    func notAuthorized() {
        stopAnimation()
        let sb = UIStoryboard(name: "Authentication", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "NotAuthorizedViewController") as! NotAuthorizedViewController
        vc.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.setValue(vc, forKey: "contentViewController")
        present(alert, animated: true, completion: nil)
    }
    
    func onFetch() {
        viewModel.validateUser()
    }
    
    func didFinishLoading() {
        stopAnimation()
    }
    
    func success() {
        stopAnimation()
        viewModel.validateUser()
    }
    
    func error(message: String) {
        stopAnimation()
        showMessage(title: "Error", message: message)
    }
}

extension LoginViewController: NotAuthorizedDelegate {
    func logout() {
        viewModel.logout()
    }
    
    func refresh() {
        showAnimation()
        viewModel.validate()
    }
    
    
}
