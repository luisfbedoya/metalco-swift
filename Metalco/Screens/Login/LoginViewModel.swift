//
//  LoginViewModel.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol LoginDelegate {
    func didFinishLoading()
    func onFetch()
    func success()
    func error(message: String)
    func notAuthorized()
}

public class LoginViewModel {
    private let useCase: AuthenticationUseCase
    private let navigator: LoginNavigator
    var delegate: LoginDelegate? = nil
    
    init(useCase: AuthenticationUseCase, navigator: LoginNavigator) {
        self.useCase = useCase
        self.navigator = navigator
    }
    
    func validate() {
        if UserService.shared.user != nil {
            self.useCase.fetch(userId: UserService.shared.user?._id ?? "", token: UserDefaults().string(forKey: "token") ?? "", onSuccess: { (data) in
                RecordService.shared.record = data.data?.record
                if data.data?.validate ?? false {
                    self.toHome()
                } else {
                    self.delegate?.notAuthorized()
                }
            }) { (error) in
                RecordService.shared.record = nil
                self.toHome()
            }
        } else {
            self.delegate?.didFinishLoading()
        }

    }
    
    func login(username: String, password: String) {
        if let token = UserDefaults().string(forKey: "token") {
            useCase.login(username: username, password: password, token: token, onSuccess: { (user) in
                if let validUser = user {
                    UserService.shared.user = validUser
                    self.delegate?.success()
                } else {
                self.delegate?.error(message: "Usuario o contraseña incorrecta.")
                }
            }) { (error) in
                self.delegate?.error(message: error.localizedDescription)
            }
        } else {
            self.delegate?.error(message: "Por favor active el permiso de notificaciones.")
        }
    }
    
    func validateUser() {
        delegate?.didFinishLoading()
        if UserService.shared.user != nil {
            self.validate()
        }
    }
    
    func toRegister() {
        navigator.toRegister()
    }
    
    func toHome() {
        if RecordService.shared.record != nil {
            navigator.toDashboard()
        } else {
            navigator.toHome()
        }
    }
    
    func logout() {
        UserService.shared.user = nil
    }
}
