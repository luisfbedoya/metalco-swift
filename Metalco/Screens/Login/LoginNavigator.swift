//
//  LoginNavigator.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import SwinjectStoryboard

public class LoginNavigator {
    let storyboard: SwinjectStoryboard
    let dashboardStoryboard: SwinjectStoryboard
    let registerStoryboard: SwinjectStoryboard
    
    init(storyboard: SwinjectStoryboard, dashboardStoryboard: SwinjectStoryboard, registerStoryboard: SwinjectStoryboard) {
        self.storyboard = storyboard
        self.dashboardStoryboard = dashboardStoryboard
        self.registerStoryboard = registerStoryboard
    }
    
    func toHome() {
        let vc = storyboard.instantiateInitialViewController()
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func toDashboard() {
        let vc = dashboardStoryboard.instantiateInitialViewController()
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    func toRegister() {
        let vc = registerStoryboard.instantiateViewController(withIdentifier: "RegisterViewController")
        UIApplication.shared.windows.first?.rootViewController?.present(vc, animated: true, completion: nil)
    }
}
