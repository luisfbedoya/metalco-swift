//
//  WorkplaceTableViewCell.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

protocol WorkplaceTableViewCellDelegate {
    func navigateTo(workspace: Workspace)
    func goToMap(workplace: Workplace)
}

class WorkplaceTableViewCell: UITableViewCell {
    @IBOutlet weak var workplaceNameLabel: UILabel!
    @IBOutlet weak var workplaceAddressLabel: UILabel!
    var delegate: WorkplaceTableViewCellDelegate? = nil
    private var workplace: Workplace? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func bind(workplace: Workplace) {
        self.workplace = workplace
        workplaceNameLabel.text = workplace.name
        workplaceAddressLabel.text = workplace.address
    }
    
    @IBAction func checkInClick(_ sender: Any) {
        if let workplace = workplace {
            delegate?.goToMap(workplace: workplace)
        }
    }
    
    @IBAction func navigateToClick(_ sender: Any) {
        if let workspace = workplace?.workSpace {
            delegate?.navigateTo(workspace: workspace)
        }
    }
}

