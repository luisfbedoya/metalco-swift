//
//  WorkplaceViewModel.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol WorkplaceDelegate {
    func loadData()
    func showError(message: String)
}

public class WorkplaceViewModel {
    let useCase: WorkplaceUseCase
    let navigator: WorkplaceNavigator
    var delegate: WorkplaceDelegate?
    var data: [Workplace] = []
    
    init(useCase: WorkplaceUseCase, navigator: WorkplaceNavigator) {
        self.useCase = useCase
        self.navigator = navigator
    }
    
    func fetch() {
        if let user = UserService.shared.user {
            self.useCase.fetch(userId: user._id, workerId: user.worker?._id ?? "", onSuccess: { (workplaces) in
                self.data = workplaces
                self.delegate?.loadData()
            }) { (error) in
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }
    
    func navigateTo(workspace: Workspace) {
        MapService.shared.navigateTo(latitude: workspace.latitude, longitude: workspace.longitude)
    }
    
    func goToMap(workplace: Workplace) {
        if let worker = UserService.shared.user?.worker {
            useCase.validateManager(workerId: worker._id, onSuccess: { (response) in
                switch response.error {
                case 0: self.navigator.toLocationValidator(workplace: workplace)
                case 1: self.delegate?.showError(message: "Sin supervisor asignado.")
                default: self.delegate?.showError(message: "Error de servidor")
                }
                
            }) { (error) in
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }
    
    func goToResourceMenu() {
        navigator.toResourceMenu()
    }
}
