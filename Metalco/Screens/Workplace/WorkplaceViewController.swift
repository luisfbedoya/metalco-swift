//
//  WorkplaceViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

class WorkplaceViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var viewModel: WorkplaceViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showAnimation()
        viewModel.fetch()
    }
    
    @IBAction func resourcesClick(_ sender: Any) {
        viewModel.goToResourceMenu()
    }
}

extension WorkplaceViewController: WorkplaceDelegate {
    func showError(message: String) {
        stopAnimation()
        showMessage(title: "Error", message: message)
    }
    
    func loadData() {
        tableView.reloadData()
        stopAnimation()
    }
    
    
}

extension WorkplaceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}

extension WorkplaceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "workplace") as? WorkplaceTableViewCell else {
            return UITableViewCell()
        }
        cell.bind(workplace: viewModel.data[indexPath.row])
        cell.delegate = self
        return cell
    }
    
}

extension WorkplaceViewController: WorkplaceTableViewCellDelegate {
    func goToMap(workplace: Workplace) {
        viewModel.goToMap(workplace: workplace)
    }
    
    func navigateTo(workspace: Workspace) {
        viewModel.navigateTo(workspace: workspace)
    }
    
    
}
