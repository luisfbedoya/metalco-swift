//
//  WorkplaceNavigator.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import SwinjectStoryboard

public class WorkplaceNavigator {
    let storyboard: SwinjectStoryboard
    
    init(storyboard: SwinjectStoryboard) {
        self.storyboard = storyboard
    }
    
    func toLocationValidator(workplace: Workplace) {
        let vc = storyboard.instantiateViewController(withIdentifier: "LocationValidatorViewController") as! LocationValidatorViewController
        vc.viewModel.workplace = workplace
        vc.modalPresentationStyle = .overFullScreen
        let tabBarController = UIApplication.shared.windows.first?.rootViewController as! TabBarViewController
        (tabBarController.selectedViewController as? MainNavigationViewController)?.present(vc, animated: true)
    }
    
    func toResourceMenu() {
        let vc = storyboard.instantiateViewController(withIdentifier: "ResourceMenuViewController") as! ResourceMenuViewController
        let tabBarController = UIApplication.shared.windows.first?.rootViewController as! TabBarViewController
        (tabBarController.selectedViewController as? MainNavigationViewController)?.pushViewController(vc, animated: true)
    }
}
