//
//  ProfileViewModel.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol ProfileDelegate {
    func loadData(user: User, data: Data?)
    func success(message: String)
    func error(message: String)
    func hideLoad()
    func showLoad()
}

public class ProfileViewModel {
    let useCase: ProfileUseCase
    let navigator: ProfileNavigator
    var delegate: ProfileDelegate?
    
    init(useCase: ProfileUseCase, navigator: ProfileNavigator) {
        self.useCase = useCase
        self.navigator = navigator
    }
    
    func fetch() {
        let userId = UserService.shared.user?._id ?? ""
        useCase.fetch(userId: userId, onSuccess: { (data) in
            if let user = UserService.shared.user {
                self.delegate?.loadData(user: user, data: data)
            }
        }) { (error) in
            self.delegate?.error(message: "Error de conexion. Verifique su conexión a internet.")
        }
    }
    
    func toLogin() {
        navigator.toLogin()
    }
    
    func changePassword(currentPassword: String, newPassword: String) {
        let userId = UserService.shared.user?._id ?? ""
        useCase.changePassword(userId: userId, currentPassword: currentPassword, newPassword: newPassword, onSuccess: { (response) in
            if response.error == 0{
                self.delegate?.success(message: "Tu contraseña se cambio con exito.")
            } else {
                self.delegate?.error(message: "Tu contraseña actual no es correcta")
            }
        }) { (error) in
            self.delegate?.error(message: "Error de conexion. Verifique su conexión a internet.")
        }
    }
    
    func uploadPhoto(with data: Data) {
        let userId = UserService.shared.user?._id ?? ""
        useCase.uploadPhoto(userId: userId, photo: data, onSuccess: { (response) in
            switch response.error {
            case 0: self.delegate?.success(message: "Se agregó foto correctamente.")
                self.fetch()
            case 2: self.delegate?.error(message: "No se detectó rostro.")
            default:
                self.delegate?.error(message: "Error de servidor")
            }
        }) { (error) in
            self.delegate?.error(message: "Error de conexion. Verifique su conexión a internet.")
        }
    }
}
