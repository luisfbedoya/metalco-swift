//
//  ProfileViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit
import AVKit

class ProfileViewController: UIViewController {
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var employeeNumberLabel: UILabel!
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var managerNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    var viewModel: ProfileViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        showAnimation()
        viewModel.fetch()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func changePasswordClick(_ sender: Any) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
        vc.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.setValue(vc, forKey: "contentViewController")
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func logoutClick(_ sender: Any) {
        viewModel.toLogin()
    }
    
    @IBAction func takePhoto(_ sender: Any) {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                DispatchQueue.main.async {
                    let vc = UIImagePickerController()
                    vc.sourceType = .camera
                    vc.cameraDevice = .front
                    vc.allowsEditing = true
                    vc.delegate = self
                    self.present(vc, animated: true)
                }
            } else {
                
            }
        }

    }
    
}

extension ProfileViewController: ProfileDelegate {
    func hideLoad() {
        stopAnimation()
    }
    
    func showLoad() {
        showAnimation()
    }
    
    func success(message: String) {
        stopAnimation()
        showMessage(title: "Exito", message: message)
    }
    
    func error(message: String) {
        stopAnimation()
        showMessage(title: "Error", message: message)
    }
    
    func loadData(user: User, data: Data?) {
        usernameLabel.text = user.username
        employeeNumberLabel.text = String(user.worker?.code ?? 0)
        employeeNameLabel.text = user.worker?.fullname
        managerNameLabel.text = user.worker?.manager?.fullname
        if let data = data {
            profileImage.image = UIImage(data: data)
        }
        stopAnimation()
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        showAnimation()
        viewModel.uploadPhoto(with: image.jpegData(compressionQuality: 1) ?? Data())
    }
}

extension ProfileViewController: ChangePasswordDelegate {
    func changePassword(currentPassword: String, newPassword: String) {
        showAnimation()
        viewModel.changePassword(currentPassword: currentPassword, newPassword: newPassword)
    }
    
}
