//
//  ProfileNavigator.swift
//  Metalco
//
//  Created by Felipe Bedoya on 07/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import SwinjectStoryboard

public class ProfileNavigator {
    let storyboard: SwinjectStoryboard
    
    init(storyboard: SwinjectStoryboard) {
        self.storyboard = storyboard
    }
    
    func toLogin() {
        UserService.shared.user = nil
        let vc = storyboard.instantiateInitialViewController()
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
