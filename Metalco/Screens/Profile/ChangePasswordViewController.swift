//
//  ChangePasswordViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 23/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

protocol ChangePasswordDelegate {
    func changePassword(currentPassword: String, newPassword: String)
}

class ChangePasswordViewController: UIViewController {
    @IBOutlet weak var currentPasswordTF: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var repeatPasswordTF: UITextField!
    var delegate: ChangePasswordDelegate? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func changePasswordClick(_ sender: Any) {
        currentPasswordTF.borderColor = .lightGray
        newPasswordTF.borderColor = .lightGray
        repeatPasswordTF.borderColor = .lightGray
        let currentPassword = currentPasswordTF.getText()
        let newPassword = newPasswordTF.getText()
        let repeatPassword = repeatPasswordTF.getText()
        if currentPassword.isEmpty {
            currentPasswordTF.borderColor = .red
            showMessage(title: "Error", message: "Contraseña vacio.")
            return
        }
        if newPassword.isEmpty {
            newPasswordTF.borderColor = .red
            showMessage(title: "Error", message: "Nueva contraseña vacio.")
            return
        }
        if repeatPassword.isEmpty {
            repeatPasswordTF.borderColor = .red
            showMessage(title: "Error", message: "Contraseña vacio.")
            return
        }
        if !newPassword.elementsEqual(repeatPassword) {
            newPasswordTF.borderColor = .red
            repeatPasswordTF.borderColor = .red
            showMessage(title: "Error", message: "Contraseñas no coinciden.")
            return
        }
        self.delegate?.changePassword(currentPassword: currentPassword, newPassword: newPassword)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
