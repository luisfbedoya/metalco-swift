//
//  WarehouseViewModel.swift
//  Metalco
//
//  Created by Felipe Bedoya on 08/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import MapKit

public protocol WarehouseDelegate {
    func loadData()
    func showError(message: String)
}

public class WarehouseViewModel {
    public var warehouse: Warehouse?
    public var resources: [Resource] = []
    public var warehouses: [Warehouse] = []
    public var delegate: WarehouseDelegate?
    public var didFind = false
    private let useCase: ResourceUseCase
    private let warehouseUseCase: WarehouseUseCase
    private var lastCode: String? = nil
    
    init(useCase: ResourceUseCase, warehouseUseCase: WarehouseUseCase) {
        self.useCase = useCase
        self.warehouseUseCase = warehouseUseCase
    }
    
    func fetch() {
        self.didFind = false
        self.warehouseUseCase.fetch(onSuccess: { (warehouses) in
            self.warehouses = warehouses
            self.useCase.resourcesByWarehouse(warehouseId: self.warehouse?._id ?? "", onSuccess: { (resources) in
                self.resources = resources
                self.delegate?.loadData()
            }) { (err) in
                print(err)
                self.delegate?.loadData()
            }
        }) { (err) in
            print(err)
            self.delegate?.loadData()
        }
    }
    
    func report(resourceId: String, reason: String) {
        useCase.report(workerId: UserService.shared.user?.worker?._id ?? "", resourceId: resourceId, reason: reason, onSuccess: { (response) in
            switch response.error {
                case 0: self.fetch()
                case 2: self.delegate?.showError(message: "Este recurso ya fue reportado")
                default: self.delegate?.showError(message: "Error de servidor")
            }
            
        }) { (err) in
            self.delegate?.showError(message: "Error de conexion")
        }
    }
    
    func transfer(resourceId: String, toType: String, toWorkerId: String, toWarehouseId: String) {
        useCase.transfer(workerId: "", warehouseId: warehouse?._id ?? "", resourceId: resourceId, fromType: "WAREHOUSE", toType: toType, toWorkerId: toWorkerId, toWarehouseId: toWarehouseId, onSuccess: { (response) in
            switch response.error {
                case 0: self.fetch()
                case 2: self.delegate?.showError(message: "Número de empleado no existe.")
                case 3: self.delegate?.showError(message: "El recurso ya tiene una transferencia pendiente.")
                default: self.delegate?.showError(message: "Error de servidor.")
            }
        }) { (err) in
            self.delegate?.showError(message: "Error de conexion")
        }
    }
    
    func update(resourceId: String, updateNumber: String, money: String) {
        useCase.update(workerId: UserService.shared.user?.worker?._id ?? "", resourceId: resourceId, money: Int(money) ?? 0, updatedValue: Double(updateNumber) ?? 0, onSuccess: { (response) in
            switch response.error {
                case 0: self.fetch()
                case 2: self.delegate?.showError(message: "Este recurso no pudo actualizarse")
                default: self.delegate?.showError(message: "Error de servidor")
            }
        }) { (err) in
            self.delegate?.showError(message: "Error de conexion")
        }
    }
    
    func maintenance(resourceId: String, reason: String) {
        useCase.maintenance(workerId: UserService.shared.user?.worker?._id ?? "", warehouseId: warehouse?._id ?? "", resourceId: resourceId, reason: reason, onSuccess: { (response) in
            switch response.error {
                case 0: self.fetch()
                case 2: self.delegate?.showError(message: "El recurso ya está en mantenimiento.")
                default: self.delegate?.showError(message: "Error de servidor.")
            }
        }) { (err) in
            self.delegate?.showError(message: "Error de conexion")
        }
        
    }
    
    func cancelMaintenance(resourceId: String) {
        useCase.cancelMaintenance(resourceId: resourceId, onSuccess: { (response) in
            switch response.error {
                case 0: self.fetch()
                default: self.delegate?.showError(message: "Error de servidor.")
            }
        }) { (err) in
            self.delegate?.showError(message: "Error de conexion")
        }
    }
    
    func finishMaintenance(resourceId: String) {
        useCase.finishMaintenance(resourceId: resourceId, onSuccess: { (response) in
            switch response.error {
                case 0: self.fetch()
                default: self.delegate?.showError(message: "Error de servidor.")
            }
        }) { (err) in
            self.delegate?.showError(message: "Error de conexion")
        }
    }
    
    func findByQr(resourceId: String) {
        if let currentLocation = MapService.shared.currentLocation {
            let coordinate = currentLocation.coordinate
            useCase.findById(resourceId: resourceId, workerId: UserService.shared.user?.worker?._id ?? "", latitude: coordinate.latitude, longitude: coordinate.longitude, onSuccess: { (resources) in
                self.resources = resources
                self.didFind = true
                self.delegate?.loadData()
            }) { (err) in
                self.delegate?.showError(message: "Error de conexion")
            }
        } else {
            self.lastCode = resourceId
            MapService.shared.delegate = self
            MapService.shared.requestCurrentLocation()
        }
    }
}

extension WarehouseViewModel: MapServiceDelegate {
    func didAuthorizeLocation() {

    }
    
    func didUpdateLocation(location: CLLocation) {
        MapService.shared.delegate = nil
        findByQr(resourceId: lastCode ?? "")
        self.lastCode = nil
        
    }
    
    func didNotAuthorizeLocation() {
        self.delegate?.showError(message: "Active el servicio de ubicación para usar esta opcion.")
    }
    
    
}
