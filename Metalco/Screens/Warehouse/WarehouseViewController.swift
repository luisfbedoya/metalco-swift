//
//  WarehouseViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 08/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit
import MercariQRScanner

class WarehouseViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scanButton: UIButton!
    public var viewModel: WarehouseViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        viewModel.fetch()
        title = viewModel.warehouse?.name
    }
    
    @IBAction func scanQrClick(_ sender: Any) {
        if viewModel.didFind {
            viewModel.fetch()
        } else {
            let qrScannerView = QRScannerView(frame: view.bounds)
            view.addSubview(qrScannerView)
            qrScannerView.configure(delegate: self)
            qrScannerView.startRunning()
        }
    }
    
    @IBAction func transferClick(_ sender: Any) {
    }
}

extension WarehouseViewController: WarehouseDelegate {
    func loadData() {
        if viewModel.didFind {
            scanButton.setTitle("Volver", for: .normal)
        } else {
            scanButton.setTitle("Escanear", for: .normal)
        }
        stopAnimation()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = UITableView.automaticDimension
        tableView.reloadData()
    }
    
    func showError(message: String) {
        stopAnimation()
        showMessage(title: "Error", message: message)
    }
}

extension WarehouseViewController: UITableViewDelegate {
    
}

extension WarehouseViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.resources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let resource = viewModel.resources[indexPath.row]
        switch resource.status {
            case .ACTIVE:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "resource") as? WarehouseResourceTableViewCell else {
                    return UITableViewCell()
                }
                cell.delegate = self
                cell.bind(resource: resource)
                return cell
            case .INACTIVE:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "inactiveResource") as? InactiveResourceTableViewCell else {
                    return UITableViewCell()
                }
                cell.bind(resource: resource)
                return cell
            case .INACTIVE_APPLICATION:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "resource") as? WarehouseResourceTableViewCell else {
                    return UITableViewCell()
                }
                cell.delegate = self
                cell.bind(resource: resource)
                return cell
            case .MAINTENANCE:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "maintenanceResource") as? ResourceMaintenanceTableViewCell else {
                    return UITableViewCell()
                }
                cell.delegate = self
                cell.bind(resource: resource)
                return cell
            case .PENDING_TRANSFER:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "resource") as? WarehouseResourceTableViewCell else {
                    return UITableViewCell()
                }
                cell.delegate = self
                cell.bind(resource: resource)
                return cell
            }
    }
}

extension WarehouseViewController: ResourceDelegate {
    func report(resource: Resource) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "ReportAlertViewController") as! ReportAlertViewController
        vc.delegate = self
        vc.resource = resource
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.setValue(vc, forKey: "contentViewController")
        present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.subviews[0].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didTapOutside)))
        }
    }
    
    func transfer(resource: Resource) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "TransferAlertViewController") as! TransferAlertViewController
        vc.delegate = self
        vc.resource = resource
        vc.warehouses = viewModel.warehouses
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.setValue(vc, forKey: "contentViewController")
        present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.subviews[0].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didTapOutside)))
        }
    }
    
    func update(resource: Resource) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "UpdateAlertViewController") as! UpdateAlertViewController
        vc.delegate = self
        vc.resource = resource
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.setValue(vc, forKey: "contentViewController")
        present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.subviews[0].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didTapOutside)))
        }
    }
    
    func maintenance(on resource: Resource) {
        let sb = UIStoryboard(name: "Home", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "MaintenanceAlertViewController") as! MaintenanceAlertViewController
        vc.delegate = self
        vc.resource = resource
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.setValue(vc, forKey: "contentViewController")
        present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            alert.view.superview?.subviews[0].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.didTapOutside)))
        }
    }
    
    func cancelMaintenance(on resource: Resource) {
        showAnimation()
        viewModel.cancelMaintenance(resourceId: resource._id)
    }
    
    func finishMaintenance(on resource: Resource) {
        showAnimation()
        viewModel.finishMaintenance(resourceId: resource._id)
    }
}

extension WarehouseViewController: ReportDelegate {
    func report(resourceId: String, reason: String) {
        showAnimation()
        viewModel.report(resourceId: resourceId, reason: reason)
    }
}

extension WarehouseViewController: TransferDelegate {
    func transfer(toType: String, toWorkerNumber: String, toWarehouseId: String, resourceId: String) {
        showAnimation()
        viewModel.transfer(resourceId: resourceId, toType: toType, toWorkerId: toWorkerNumber, toWarehouseId: toWarehouseId)
    }
}

extension WarehouseViewController: MaintenanceDelegate {
    func maintenance(to resource: Resource, reason: String) {
        showAnimation()
        viewModel.maintenance(resourceId: resource._id, reason: reason)
    }
}

extension WarehouseViewController: UpdateDelegate {
    func update(resourceId: String, currentRead: String, expense: String) {
        showAnimation()
        viewModel.update(resourceId: resourceId, updateNumber: currentRead, money: expense)
    }
}

extension WarehouseViewController: QRScannerViewDelegate {
    func qrScannerView(_ qrScannerView: QRScannerView, didFailure error: QRScannerError) {
        print(error)
    }

    func qrScannerView(_ qrScannerView: QRScannerView, didSuccess code: String) {
        qrScannerView.stopRunning()
        qrScannerView.removeFromSuperview()
        showAnimation()
        viewModel.findByQr(resourceId: code)
    }
}
