//
//  MaintenanceAlertViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 10/01/21.
//  Copyright © 2021 Felipe Bedoya. All rights reserved.
//

import UIKit

protocol MaintenanceDelegate {
    func maintenance(to resource: Resource, reason: String)
}

class MaintenanceAlertViewController: UIViewController {
    @IBOutlet weak var resourceNameLabel: UILabel!
    @IBOutlet weak var reasonTextField: UITextView!
    var resource: Resource? = nil
    var delegate: MaintenanceDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func maintenanceClick(_ sender: Any) {
        if let resource = self.resource {
            self.delegate?.maintenance(to: resource, reason: reasonTextField.text)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}
