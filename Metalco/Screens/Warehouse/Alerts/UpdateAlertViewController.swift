//
//  UpdateAlertViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 10/01/21.
//  Copyright © 2021 Felipe Bedoya. All rights reserved.
//

import UIKit

protocol UpdateDelegate {
    func update(resourceId: String, currentRead: String, expense: String)
}

class UpdateAlertViewController: UIViewController {
    @IBOutlet weak var resourceNameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var frequencyProgress: UIProgressView!
    @IBOutlet weak var currentTextField: UITextField!
    @IBOutlet weak var expenseTextField: UITextField!
    @IBOutlet weak var currentLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var frequencyLabel: UILabel!
    var resource: Resource? = nil
    var delegate: UpdateDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.typeLabel.text = resource?.typeMaintenanceString
        let currentMaintenance = Float(resource?.currentMaintenance ?? 0)
        let maintenanceFrequency = Float(resource?.maintenanceFrequency ?? 1)
        self.frequencyProgress.setProgress(currentMaintenance/maintenanceFrequency, animated: true)
        if self.frequencyProgress.progress > 0 {
            self.frequencyProgress.progressTintColor = .green
        }
        if self.frequencyProgress.progress > 0.25 {
            self.frequencyProgress.progressTintColor = .yellow
        }
        if self.frequencyProgress.progress > 0.75 {
            self.frequencyProgress.progressTintColor = .red
        }
        if (resource?.typeMaintenance == .NONE) {
            self.currentLabel.text = ""
            self.frequencyLabel.text = ""
            self.totalLabel.text = ""
        } else {
            self.currentLabel.text = String(resource?.currentMaintenance ?? 0)
            self.frequencyLabel.text = String(resource?.maintenanceFrequency ?? 0)
            self.totalLabel.text = String(resource?.maintenanceNumber ?? 0)
        }
    }
    

    @IBAction func updateClick(_ sender: Any) {
        currentTextField.borderColor = .lightGray
        expenseTextField.borderColor = .lightGray
        if currentTextField.text?.isEmpty ?? true {
            currentTextField.borderColor = .red
            return
        }
        if expenseTextField.text?.isEmpty ?? true {
            expenseTextField.borderColor = .red
            return
        }
        self.delegate?.update(resourceId: resource?._id ?? "", currentRead: currentTextField.text ?? "", expense: expenseTextField.text ?? "")
        self.dismiss(animated: true)
    }
}
