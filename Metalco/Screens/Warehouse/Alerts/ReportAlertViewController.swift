//
//  ReportAlertViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 10/01/21.
//  Copyright © 2021 Felipe Bedoya. All rights reserved.
//

import UIKit

public protocol ReportDelegate {
    func report(resourceId: String, reason: String)
}

class ReportAlertViewController: UIViewController {
    @IBOutlet weak var resourceNameLabel: UILabel!
    @IBOutlet weak var reasonTextField: UITextView!
    var resource: Resource? = nil
    var delegate: ReportDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resourceNameLabel.text = resource?.name
        // Do any additional setup after loading the view.
    }
    
    @IBAction func reportClick(_ sender: Any) {
        delegate?.report(resourceId: resource?._id ?? "", reason: reasonTextField.text)
        self.dismiss(animated: true, completion: nil)
    }
}
