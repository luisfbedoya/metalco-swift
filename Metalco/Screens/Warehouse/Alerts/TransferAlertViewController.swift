//
//  TransferAlertViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 10/01/21.
//  Copyright © 2021 Felipe Bedoya. All rights reserved.
//

import UIKit

public protocol TransferDelegate {
    func transfer(toType: String, toWorkerNumber: String, toWarehouseId: String, resourceId: String)
}

class TransferAlertViewController: UIViewController {
    @IBOutlet weak var whereTextField: UITextField!
    @IBOutlet weak var whereId: UITextField!
    @IBOutlet var wherePicker: UIPickerView!
    @IBOutlet var warehousePicker: UIPickerView!
    @IBOutlet weak var resourceNameLabel: UILabel!
    let whereOptions = ["Empleado", "Almacen"]
    var warehouses: [Warehouse] = []
    var resource: Resource? = nil
    var delegate: TransferDelegate? = nil
    
    var toWhereClicked = false
    var warehouseClicked = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resourceNameLabel.text = resource?.name
        whereTextField.inputView = wherePicker
    }
    
    @IBAction func transferClick(_ sender: Any) {
        let whereIndex = wherePicker.selectedRow(inComponent: 0)        
        let toType = whereIndex == 1 ? "WAREHOUSE" : "WORKER"
        let workerNumber = whereId.text ?? ""
        let index = warehousePicker.selectedRow(inComponent: 0)
        let warehouseId = warehouses[index]._id
        delegate?.transfer(toType: toType, toWorkerNumber: workerNumber, toWarehouseId: warehouseId, resourceId: resource?._id ?? "")
        self.dismiss(animated: true)
    }
    
    @IBAction func toWhereClicked(_ sender: Any) {
        if !toWhereClicked {
            wherePicker.selectRow(0, inComponent: 0, animated: false)
            pickerView(wherePicker, didSelectRow: 0, inComponent: 0)
            toWhereClicked = true
        }
    }
    
    @IBAction func whereIdClicked(_ sender: Any) {
        let whereIndex = wherePicker.selectedRow(inComponent: 0)
        if !warehouseClicked && whereIndex != 0{
            warehousePicker.selectRow(0, inComponent: 0, animated: false)
            pickerView(warehousePicker, didSelectRow: 0, inComponent: 0)
            warehouseClicked = true
        }
    }
}

extension TransferAlertViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return 2
        } else {
            return warehouses.count
        }
    }
}

extension TransferAlertViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return whereOptions[row]
        } else {
            return warehouses[row].name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            whereTextField.text = whereOptions[row]
            whereId.text = ""
            if row == 1 {
                whereId.inputView = warehousePicker
                whereId.placeholder = "Seleccione un almacén"
                warehouseClicked = false
            } else {
                whereId.inputView = nil
                whereId.placeholder = "Número de empleado"
            }
        } else {
            whereId.text = warehouses[row].name
        }
    }
}
