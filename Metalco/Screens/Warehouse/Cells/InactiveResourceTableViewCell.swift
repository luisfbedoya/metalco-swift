//
//  InactiveResourceTableViewCell.swift
//  Metalco
//
//  Created by Felipe Bedoya on 04/01/21.
//  Copyright © 2021 Felipe Bedoya. All rights reserved.
//

import UIKit

class InactiveResourceTableViewCell: UITableViewCell {
    private var resource: Resource?
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(resource: Resource) {
        self.resource = resource
        self.nameLabel.text = resource.name
        self.statusLabel.text = resource.statusString
        self.statusLabel.textColor = .red
    }

}
