//
//  WarehouseResourceTableViewCell.swift
//  Metalco
//
//  Created by Felipe Bedoya on 29/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

public protocol ResourceDelegate {
    func report(resource: Resource)
    func transfer(resource: Resource)
    func update(resource: Resource)
    func maintenance(on resource: Resource)
    func cancelMaintenance(on resource: Resource)
    func finishMaintenance(on resource: Resource)
}

class WarehouseResourceTableViewCell: UITableViewCell {
    private var resource: Resource?
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var frequencyProgress: UIProgressView!
    @IBOutlet weak var currentMaintenanceLabel: UILabel!
    @IBOutlet weak var frequencyLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalTextLabel: UILabel!
    @IBOutlet weak var reportButton: UIButton!
    @IBOutlet weak var maintenanceButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    var delegate: ResourceDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(resource: Resource, showMaintenance: Bool = true) {
        self.resource = resource
        self.nameLabel.text = resource.name
        self.statusLabel.text = resource.statusString
        switch resource.status {
        case .ACTIVE:
            self.statusLabel.textColor = .green
        case .INACTIVE:
            self.statusLabel.textColor = .red
        case .INACTIVE_APPLICATION:
            self.statusLabel.textColor = .red
        case .MAINTENANCE:
            self.statusLabel.textColor = .yellow
        case .PENDING_TRANSFER:
            self.statusLabel.textColor = .yellow
        }
        self.typeLabel.text = resource.typeMaintenanceString
        let currentMaintenance = Float(resource.currentMaintenance ?? 0)
        let maintenanceFrequency = Float(resource.maintenanceFrequency ?? 1)
        self.frequencyProgress.setProgress(currentMaintenance/maintenanceFrequency, animated: true)
        if self.frequencyProgress.progress > 0 {
            self.frequencyProgress.progressTintColor = .green
        }
        if self.frequencyProgress.progress > 0.25 {
            self.frequencyProgress.progressTintColor = .yellow
        }
        if self.frequencyProgress.progress > 0.75 {
            self.frequencyProgress.progressTintColor = .red
        }
        if (resource.typeMaintenance == .NONE) {
            self.currentMaintenanceLabel.text = ""
            self.frequencyLabel.text = ""
            self.totalLabel.text = ""
            self.totalTextLabel.isHidden = true
            self.maintenanceButton.isHidden = true
            self.updateButton.isHidden = true
        } else {
            self.currentMaintenanceLabel.text = String(resource.currentMaintenance ?? 0)
            self.frequencyLabel.text = String(resource.maintenanceFrequency ?? 0)
            self.totalLabel.text = String(resource.maintenanceNumber ?? 0)
            self.totalTextLabel.isHidden = false
            self.maintenanceButton.isHidden = !showMaintenance || false
            self.updateButton.isHidden = false
        }
        
    }
    
    @IBAction func reportClick(_ sender: Any) {
        if let resource = resource {
            delegate?.report(resource: resource)
        }
    }
    
    @IBAction func transferClick(_ sender: Any) {
        if let resource = resource {
            delegate?.transfer(resource: resource)
        }
    }
    
    @IBAction func updateClick(_ sender: Any) {
        if let resource = resource {
            delegate?.update(resource: resource)
        }
    }
    
    @IBAction func maintenanceClick(_ sender: Any) {
        if let resource = resource {
            delegate?.maintenance(on: resource)
        }
    }
}
