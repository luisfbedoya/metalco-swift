//
//  ResourceMaintenanceTableViewCell.swift
//  Metalco
//
//  Created by Felipe Bedoya on 29/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

class ResourceMaintenanceTableViewCell: UITableViewCell {
    private var resource: Resource?
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var frequencyProgress: UIProgressView!
    @IBOutlet weak var currentMaintenanceLabel: UILabel!
    @IBOutlet weak var frequencyLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalTextLabel: UILabel!
    var delegate: ResourceDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func bind(resource: Resource) {
        self.resource = resource
        self.nameLabel.text = resource.name
        self.statusLabel.text = resource.statusString
        self.statusLabel.textColor = .yellow
        self.typeLabel.text = resource.typeMaintenanceString
        let currentMaintenance = Float(resource.currentMaintenance ?? 0)
        let maintenanceFrequency = Float(resource.maintenanceFrequency ?? 1)
        self.frequencyProgress.setProgress(currentMaintenance/maintenanceFrequency, animated: true)
        if self.frequencyProgress.progress > 0 {
            self.frequencyProgress.progressTintColor = .green
        }
        if self.frequencyProgress.progress > 0.25 {
            self.frequencyProgress.progressTintColor = .yellow
        }
        if self.frequencyProgress.progress > 0.75 {
            self.frequencyProgress.progressTintColor = .red
        }
        if (resource.typeMaintenance == .NONE) {
            self.currentMaintenanceLabel.text = ""
            self.frequencyLabel.text = ""
            self.totalLabel.text = ""
            self.totalTextLabel.isHidden = true
        } else {
            self.currentMaintenanceLabel.text = String(resource.currentMaintenance ?? 0)
            self.frequencyLabel.text = String(resource.maintenanceFrequency ?? 0)
            self.totalLabel.text = String(resource.maintenanceNumber ?? 0)
            self.totalTextLabel.isHidden = false
        }
    }
    
    @IBAction func cancelClick(_ sender: Any) {
        if let resource = resource {
            delegate?.cancelMaintenance(on: resource)
        }
    }
    
    @IBAction func finishClick(_ sender: Any) {
        if let resource = resource {
            delegate?.finishMaintenance(on: resource)
        }
    }
}
