//
//  LocationValidatorNavigator.swift
//  Metalco
//
//  Created by Felipe Bedoya on 16/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import SwinjectStoryboard

public class LocationValidatorNavigator {
    let storyboard: SwinjectStoryboard
    
    init(storyboard: SwinjectStoryboard) {
        self.storyboard = storyboard
    }
    
    func toDashboard(workplace: Workplace) {
        let vc = storyboard.instantiateInitialViewController() as! DashboardViewController
        vc.workplace = workplace
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
