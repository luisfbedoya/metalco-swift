//
//  LocationValidatorViewModel.swift
//  Metalco
//
//  Created by Felipe Bedoya on 14/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import GoogleMaps

protocol LocationValidatorDelegate {
    func showPermissionError()
    func loadMap()
    func updateMap(with location: CLLocation)
    func isInside()
    func isOutside()
    func hideLoad()
    func showFaceError()
    func showLoad()
    func showError(message: String)
}
public class LocationValidatorViewModel {
    var workplace: Workplace?
    var delegate: LocationValidatorDelegate?
    let useCase: LocationValidationUseCase
    let navigator: LocationValidatorNavigator
    
    init(useCase: LocationValidationUseCase, navigator: LocationValidatorNavigator) {
        self.useCase = useCase
        self.navigator = navigator
    }
    
    func fetch() {
        MapService.shared.delegate = self
        MapService.shared.requestCurrentLocation()
    }
    
    func checkIn() {
        if let workspace = workplace?.workSpace {
            if MapService.shared.isInside(workspace: workspace) {
                delegate?.isInside()
            } else {
                delegate?.isOutside()
            }
        }
    }
    
    func uploadPhoto(with data: Data) {
        if let user = UserService.shared.user {
            delegate?.showLoad()
            useCase.detectFace(userId: user._id, data: data, onSuccess: { (response) in
                if response.error == 0 {
                    self.delegate?.hideLoad()
                    self.navigator.toDashboard(workplace: self.workplace!)
                } else {
                    self.delegate?.showFaceError()
                }
            }) { (error) in
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }
}

extension LocationValidatorViewModel: MapServiceDelegate {
    func didNotAuthorizeLocation() {
        delegate?.showPermissionError()
    }
    
    func didAuthorizeLocation() {
        delegate?.loadMap()
    }
    
    func didUpdateLocation(location: CLLocation) {
        delegate?.updateMap(with: location)
    }
}
