//
//  LocationValidatorViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 14/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit
import AVKit
import GoogleMaps

class LocationValidatorViewController: UIViewController {
    var viewModel: LocationValidatorViewModel!
    var mapView: GMSMapView!
    @IBOutlet var container: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        showAnimation()
        viewModel.fetch()

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func backClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func checkInClick(_ sender: Any) {
        viewModel.checkIn()
    }
}

extension LocationValidatorViewController: LocationValidatorDelegate {
    func showError(message: String) {
        stopAnimation()
        showMessage(title: "Error", message: message)
    }
    
    func showFaceError() {
        stopAnimation()
        showMessage(title: "Error", message: "No se pudo verificar rostro. Intente nuevamente")
    }
    
    func showPermissionError() {
        stopAnimation()
        showMessage(title: "Error", message: "Por favor active el permiso de localizacion para continuar.") { (action) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
             }
        }
    }
    
    
    func updateMap(with location: CLLocation) {
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 17)
        mapView.animate(to: camera)
    }
    
    func loadMap() {
        stopAnimation()
        let workLocation = CLLocationCoordinate2D(latitude: viewModel.workplace?.workSpace?.latitude ?? 0, longitude: viewModel.workplace?.workSpace?.longitude ?? 0)
        let camera = GMSCameraPosition.camera(withLatitude: workLocation.latitude, longitude: workLocation.longitude, zoom: 14)
        mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        mapView.setMinZoom(10, maxZoom: 18)
        mapView.animate(to: camera)
        let marker = GMSMarker(position: workLocation)
        marker.icon = UIImage(named: "")
        marker.title = viewModel.workplace?.name
        marker.snippet = viewModel.workplace?.address
        marker.map = mapView
        let radius = GMSCircle(position: workLocation, radius: CLLocationDistance(viewModel.workplace?.workSpace?.radiusMeters ?? 1))
        radius.fillColor = UIColor(hexString: "#14008577")
        radius.map = mapView
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
        self.embed(view: mapView, into: container)
    }
    
    func isInside() {
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                DispatchQueue.main.async {
                    let vc = UIImagePickerController()
                    vc.sourceType = .camera
                    vc.cameraDevice = .front
                    vc.allowsEditing = true
                    vc.delegate = self
                    self.present(vc, animated: true)
                }
            } else {
                
            }
        }
    }
    
    func isOutside() {
        stopAnimation()
        showMessage(title: "Fuera de Geocerca", message: "Te encuentras fuera de la ubicación")
    }
    
    func hideLoad() {
        stopAnimation()
    }
    
    func showLoad() {
        showAnimation()
    }
}


extension LocationValidatorViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        viewModel.uploadPhoto(with: image.jpegData(compressionQuality: 1) ?? Data())
    }
}
