//
//  HistoryViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: HistoryViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        showAnimation()
        viewModel.fetch()
    }
}

extension HistoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }
}

extension HistoryViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "record") as? RecordTableViewCell else {
            return UITableViewCell()
        }
        cell.bind(record: viewModel.data[indexPath.row])
        return cell
    }
    
}

extension HistoryViewController: HistoryDelegate {
    func reloadTable() {
        tableView.reloadData()
        stopAnimation()
    }
    
    func error(error: NetworkError) {
        stopAnimation()
        showMessage(title: "Error", message: error.localizedDescription)
    }
    
    
}
