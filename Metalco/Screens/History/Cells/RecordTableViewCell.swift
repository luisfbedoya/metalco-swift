//
//  RecordTableViewCell.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

class RecordTableViewCell: UITableViewCell {
    @IBOutlet weak var managerLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var checkInLabel: UILabel!
    @IBOutlet weak var checkoutLabel: UILabel!
    @IBOutlet weak var statusImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func bind(record: Record) {
        managerLabel.text = record.manager?.fullname
        dateLabel.text = record.date?.toString(like: "dd/MM/YYYY")
        checkInLabel.text = record.inDate?.toString(like: "h:mm a")
        checkoutLabel.text = record.outDate?.toString(like: "h:mm a")
        switch record.status {
        case "CREATED":
            statusImageView.image = UIImage(named: "ic_hourglass")
        case "FINISHED":
            statusImageView.image = UIImage(named: "ic_check_circle")
        case "INVALID":
            statusImageView.image = UIImage(named: "ic_error")
        default:
            statusImageView.image = UIImage(named: "ic_error")
        }
    }

}
