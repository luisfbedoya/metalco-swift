//
//  HistoryViewModel.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol HistoryDelegate {
    func reloadTable()
    func error(error: NetworkError)
}

public class HistoryViewModel {
    private let useCase: HistoryUseCase
    var data: [Record] = []
    var delegate: HistoryDelegate? = nil
    
    init(useCase: HistoryUseCase) {
        self.useCase = useCase
    }
    
    func fetch() {
        if let user = UserService.shared.user {
            useCase.fetch(userId: user._id, workerId: user.worker?._id ?? "", onSuccess: { (records) in
                self.data = records
                self.data.sort { (r1, r2) -> Bool in
                    r1.inDate ?? Date() > r2.inDate ?? Date()
                }
                self.delegate?.reloadTable()
            }) { (error) in
                self.delegate?.error(error: NetworkError.message("Error de conexion. Verifique su conexion a internet."))
            }
        }
    }
}
