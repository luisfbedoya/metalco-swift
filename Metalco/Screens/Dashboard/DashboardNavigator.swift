//
//  DashboardNavigator.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import SwinjectStoryboard

public class DashboardNavigator {
    
    let storyboard: SwinjectStoryboard
    
    init(storyboard: SwinjectStoryboard) {
        self.storyboard = storyboard
    }
    
    func toHome() {
        let vc = storyboard.instantiateInitialViewController()
        UIApplication.shared.windows.first?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
