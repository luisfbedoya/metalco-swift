//
//  DashboardLocationTableViewCell.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

class DashboardLocationTableViewCell: UITableViewCell {
    var record: Record? = nil
    @IBOutlet weak var locationNameLabel: UILabel!
    @IBOutlet weak var managerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func bind(record: Record) {
        self.record = record
        print(record)
        locationNameLabel.text = record.workPlace?.name
        managerLabel.text = record.manager?.fullname
    }
}
