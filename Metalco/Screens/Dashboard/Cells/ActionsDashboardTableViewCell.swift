//
//  ActionsDashboardTableViewCell.swift
//  Metalco
//
//  Created by Felipe Bedoya on 21/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

protocol ActionsDashboardDelegate {
    func onBreak()
    func onEmployeeRegister()
}

class ActionsDashboardTableViewCell: UITableViewCell {
    
    var delegate: ActionsDashboardDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func onBreakClick(_ sender: Any) {
        delegate?.onBreak()
    }
    
    @IBAction func employeeRegisterClick(_ sender: Any) {
        delegate?.onEmployeeRegister()
    }
}
