//
//  DashboardRegisterTableViewCell.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

class DashboardRegisterTableViewCell: UITableViewCell {

    @IBOutlet weak var checkInHourLabel: UILabel!
    @IBOutlet weak var checkOutFoodLabel: UILabel!
    @IBOutlet weak var checkInFoodLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bind(record: Record) {
        checkInHourLabel.text = record.inDate?.toString(like: "h:mm a") ?? "--:--"
        checkOutFoodLabel.text = record.outBreakDate?.toString(like: "h:mm a") ?? "--:--"
        checkInFoodLabel.text = record.inBreakDate?.toString(like: "h:mm a") ?? "--:--"
    }

}
