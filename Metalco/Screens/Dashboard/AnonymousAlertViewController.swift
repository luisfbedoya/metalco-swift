//
//  AnonymousAlertViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 22/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

protocol AnonymousAlertDelegate {
    func checkInAnonymous(username: String)
    func checkOutAnonymous(username: String)
}

class AnonymousAlertViewController: UIViewController {
    @IBOutlet weak var usernameTF: UITextField!
    var delegate: AnonymousAlertDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func checkInClick(_ sender: Any) {
        let username = usernameTF.getText()
        if validate(username: username) {
            self.delegate?.checkInAnonymous(username: username)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func checkoutClick(_ sender: Any) {
        let username = usernameTF.getText()
        if validate(username: username) {
            self.delegate?.checkOutAnonymous(username: username)
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func cancelClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func validate(username: String) -> Bool{
        if username.isEmpty {
            usernameTF.borderColor = .red
            showMessage(title: "Error", message: "Nombre de usuario vacio")
            return false
        } else {
            return true
        }
    }
}
