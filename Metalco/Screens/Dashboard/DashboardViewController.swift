//
//  DashboardViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit
import AVKit

class DashboardViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: DashboardViewModel!
    var workplace: Workplace!
    var currentCameraSelection: CameraSelection = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        viewModel.workplace = workplace
        showAnimation()
        viewModel.fetch()
    }
    
    func checkBreak() {
        if viewModel.data?.inBreakDate != nil && viewModel.data?.outBreakDate != nil {
            showMessage(title: "Error", message: "Ya has registrado tu hora de comida.")
            return
        }
        showBreakAlert(outBreak: viewModel.data?.outBreakDate == nil)
    }
    
    private func showBreakAlert(outBreak: Bool) {
        let breakType = outBreak == true ? "salida" : "entrada"
        let alertController = UIAlertController(title: "Registrar \(breakType) de comida", message: "Deseas registrar tu \(breakType) de comida?", preferredStyle: UIAlertController.Style.alert)
        let changeAction = UIAlertAction(title: "Registrar", style: .default) { (alert) in
            self.showAnimation()
            if outBreak {
                self.viewModel.checkOutBreak()
            } else {
                self.viewModel.checkInBreak()
            }
        }
        let dismissAction = UIAlertAction(title: "Cancelar", style: .cancel) { (alert) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(changeAction)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func checkOutClick(_ sender: Any) {
        showCheckoutAlert(inside: viewModel.validateLocation())
    }
    
    func showCheckoutAlert(inside: Bool) {
        let title = "Registrar Salida"
        let message = "Estás seguro que deseas registrar tu salida?"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let changeAction = UIAlertAction(title: "Registrar", style: .default) { (alert) in
            AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
                if response {
                    self.currentCameraSelection = inside ? .checkOut : .invalidate
                    DispatchQueue.main.async {
                        let vc = UIImagePickerController()
                        vc.sourceType = .camera
                        vc.cameraDevice = .front
                        vc.allowsEditing = true
                        vc.delegate = self
                        self.present(vc, animated: true)
                    }
                } else {
                    self.showPermissionErrorMessage(title: "Error", message: "Por favor active el permiso de cámara para continuar.")
                }
            }
        }
        let dismissAction = UIAlertAction(title: "Cancelar", style: .cancel) { (alert) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(changeAction)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showInvalidateAlert() {
        let title = "Fuera de geocerca"
        let message = "Estás seguro que deseas registrar tu salida? (Tu registro quedará inválido)"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let changeAction = UIAlertAction(title: "Si", style: .default) { (alert) in
            self.viewModel.invalidate()
        }
        let dismissAction = UIAlertAction(title: "No", style: .cancel) { (alert) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(changeAction)
        alertController.addAction(dismissAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension DashboardViewController: DashboardDelegate {
    func success() {
        stopAnimation()
    }
    
    func showSuccess(message: String) {
        stopAnimation()
        showMessage(title: "Exito", message: message)
    }
    
    func checkInCamera() {
        stopAnimation()
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                self.currentCameraSelection = .checkInAnonymous
                DispatchQueue.main.async {
                    let vc = UIImagePickerController()
                    vc.sourceType = .camera
                    vc.cameraDevice = .front
                    vc.allowsEditing = true
                    vc.delegate = self
                    self.present(vc, animated: true)
                }
                
            } else {
                self.showPermissionErrorMessage(title: "Error", message: "Por favor active el permiso de cámara para continuar.")
            }
        }
    }
    
    func checkOutCamera() {
        stopAnimation()
        AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
            if response {
                self.currentCameraSelection = .checkOutAnonymous
                DispatchQueue.main.async {
                    let vc = UIImagePickerController()
                    vc.sourceType = .camera
                    vc.cameraDevice = .front
                    vc.allowsEditing = true
                    vc.delegate = self
                    self.present(vc, animated: true)
                }
            } else {
                self.showPermissionErrorMessage(title: "Error", message: "Por favor active el permiso de cámara para continuar.")
            }
        }
    }
    
    func showError(message: String) {
        stopAnimation()
        showMessage(title: "Error", message: message)
    }
    
    func reloadTable() {
        tableView.reloadData()
        stopAnimation()
    }
}

extension DashboardViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0: return 100
        case 1: return 180
        case 2: return 200
        default: return UITableView.automaticDimension
        }
    }
}

extension DashboardViewController: ActionsDashboardDelegate {
    func onBreak() {
        checkBreak()
    }
    
    func onEmployeeRegister() {
        let sb = UIStoryboard(name: "Dashboard", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "AnonymousAlertViewController") as! AnonymousAlertViewController
        vc.delegate = self
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        alert.setValue(vc, forKey: "contentViewController")
        present(alert, animated: true, completion: nil)
    }
}

extension DashboardViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        let data = image.jpegData(compressionQuality: 1) ?? Data()
        showAnimation()
        
        switch currentCameraSelection {
        case .none:
            break
        case .checkOut:
            viewModel.uploadFace(data: data)
        case .checkInAnonymous:
            viewModel.detectAnonymousFace(data: data)
        case .checkOutAnonymous:
            viewModel.uploadAnonymousFace(data: data)
        case .invalidate:
            stopAnimation()
            showInvalidateAlert()
        }
    }
}

extension DashboardViewController: AnonymousAlertDelegate {
    func checkInAnonymous(username: String) {
        showAnimation()
        viewModel.validateUserCheckIn(username: username)
    }
    
    func checkOutAnonymous(username: String) {
        showAnimation()
        viewModel.validateUserCheckOut(username: username)
    }
}

extension DashboardViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.data != nil {
            return 3
        } else {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "actions") as? ActionsDashboardTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "location") as? DashboardLocationTableViewCell else {
                return UITableViewCell()
            }
            if let record = viewModel.data {
                cell.bind(record: record)
            }
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "register") as? DashboardRegisterTableViewCell else {
                return UITableViewCell()
            }
            if let record = viewModel.data {
                cell.bind(record: record)
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    
}
