//
//  CameraSelection.swift
//  Metalco
//
//  Created by Felipe Bedoya on 22/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

enum CameraSelection {
    case none
    case checkOut
    case checkInAnonymous
    case checkOutAnonymous
    case invalidate
}
