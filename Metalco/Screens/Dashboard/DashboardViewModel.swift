//
//  DashboardViewModel.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol DashboardDelegate {
    func reloadTable()
    func showError(message: String)
    func checkInCamera()
    func checkOutCamera()
    func showSuccess(message: String)
    func success()
}

public class DashboardViewModel {
    let useCase: DashboardUseCase
    let user: User
    var coworker: User?
    let navigator: DashboardNavigator
    var workplace: Workplace? = nil
    var data: Record? = nil
    var delegate: DashboardDelegate?
    
    init (useCase: DashboardUseCase, navigator: DashboardNavigator) {
        self.useCase = useCase
        self.user = UserService.shared.user!
        self.navigator = navigator
        MapService.shared.requestCurrentLocation()
    }
    
    func fetch() {
        if let record = RecordService.shared.record {
            useCase.fetchRecord(userId: user._id, workerId: user.worker?._id ?? "", recordId: record._id, onSuccess: { (record) in
                self.data = record
                self.workplace = record.workPlace
                RecordService.shared.record = record
                self.delegate?.reloadTable()
            }) { (error) in
                self.delegate?.showError(message: error.localizedDescription)
            }
        } else {
            self.create()
        }
    }
    
    func create() {
        useCase.createRecord(userId: user._id, workerId: user.worker?._id ?? "", workplaceId: workplace?._id ?? "", onSuccess: { (record) in
            self.data = record
            self.data?.workPlace = self.workplace
            RecordService.shared.record = self.data
            self.delegate?.reloadTable()
        }) { (error) in
            self.delegate?.showError(message: error.localizedDescription)
        }
    }
    
    func finish() {
        useCase.finishRecord(userId: user._id, workerId: user.worker?._id ?? "", recordId: data?._id ?? "", onSuccess: { (response) in
            switch response.error {
            case 0:
                self.delegate?.success()
                RecordService.shared.record = nil
                self.goToHome()
            case 1:
                self.delegate?.showError(message: "Error de servidor")
            case 2:
                self.delegate?.showError(message: "Número de empleado no existe")
            case 3:
                self.delegate?.showError(message: "No existe registro para terminar")
                self.goToHome()
            default:
                self.delegate?.showError(message: "Error de servidor")
            }
        }) { (error) in
            self.delegate?.showError(message: error.localizedDescription)
        }
    }
    
    func checkInBreak() {
        useCase.checkInBreak(userId: user._id, workerId: user.worker?._id ?? "", recordId: data?._id ?? "", onSuccess: { (record) in
            self.data = record
            RecordService.shared.record = record
            self.delegate?.reloadTable()
        }) { (error) in
            self.delegate?.showError(message: error.localizedDescription)
        }
    }
    
    func checkOutBreak() {
        useCase.checkOutBreak(userId: user._id, workerId: user.worker?._id ?? "", recordId: data?._id ?? "", onSuccess: { (record) in
            self.data = record
            RecordService.shared.record = record
            self.delegate?.reloadTable()
        }) { (error) in
            self.delegate?.showError(message: error.localizedDescription)
        }
    }
    
    func goToHome() {
        navigator.toHome()
    }
    
    func uploadFace(data: Data) {
        if !validateLocation() {
            
        }
        useCase.uploadFace(userId: user._id, data: data, onSuccess: { (response) in
            switch response.error {
            case 0:
                self.finish()
            case 2:
                self.delegate?.showError(message: "No se pudo verificar rostro, intente nuevamente.")
            case 3:
                self.delegate?.showError(message: "Número de empleado no existe.")
            default:
                self.delegate?.showError(message: "Error de servidor")
            }
        }) { (error) in
            print(error)
            self.delegate?.showError(message: error.localizedDescription)
        }
    }
    
    func validateLocation() -> Bool {
        if let workspace = workplace?.workSpace {
            return MapService.shared.isInside(workspace: workspace)
        } else {
            return false
        }
    }
    
    func validateUserCheckIn(username: String) {
        if !validateLocation() {
            self.delegate?.showError(message: "No te encuentras en la ubicación")
            return
        }
        useCase.validateAnonymousUser(username: username, onSuccess: { (data) in
            if data.data._id == self.user._id {
                self.delegate?.showError(message: "El nombre de usuario es el mismo al tuyo.")
            } else {
                self.coworker = data.data
                self.delegate?.checkInCamera()
            }
        }) { (error) in
            self.delegate?.showError(message: error.localizedDescription)
        }
    }
    
    func validateUserCheckOut(username: String) {
        if !validateLocation() {
            self.delegate?.showError(message: "No te encuentras en la ubicación")
            return
        }
        useCase.validateAnonymousUser(username: username, onSuccess: { (data) in
            if data.data._id == self.user._id {
                self.delegate?.showError(message: "El nombre de usuario es el mismo al tuyo.")
            } else {
                self.coworker = data.data
                self.delegate?.checkOutCamera()
            }
        }) { (error) in
            self.delegate?.showError(message: error.localizedDescription)
        }
    }
    
    func detectAnonymousFace(data: Data) {
        if let coworker = self.coworker {
            useCase.validateFace(userId: coworker._id, data: data, onSuccess: { (response) in
                switch response.error {
                case 0:
                    self.checkInAnonymous()
                case 2:
                    self.delegate?.showError(message: "No se pudo verificar rostro, intentelo nuevamente.")
                case 3:
                    self.delegate?.showError(message: "Número de empleado no existe.")
                default:
                    self.delegate?.showError(message: "Error de servidor")
                }
            }) { (error) in
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }
    
    func uploadAnonymousFace(data: Data) {
        if let coworker = self.coworker {
            useCase.uploadFace(userId: coworker._id, data: data, onSuccess: { (response) in
                switch response.error {
                case 0:
                    self.checkOutAnonymous()
                case 2:
                    self.delegate?.showError(message: "No se pudo verificar rostro, intentelo nuevamente.")
                case 3:
                    self.delegate?.showError(message: "Número de empleado no existe.")
                default:
                    self.delegate?.showError(message: "Error de servidor")
                }
            }) { (error) in
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }
    
    func checkInAnonymous() {
        if let coworker = self.coworker {
            self.useCase.createAnonymousRecord(userId: user._id, workerId: coworker.worker?._id ?? "", workplaceId: workplace?._id ?? "", onSuccess: { (record) in
                self.delegate?.showSuccess(message: "Entrada de Empleado registrada.")
                self.coworker = nil
            }) { (error) in
                self.delegate?.showError(message: error.localizedDescription)
            }
        } else {
            self.delegate?.showError(message: "Error de servidor.")
        }
    }
    
    func checkOutAnonymous() {
        if let coworker = self.coworker {
            useCase.finishAnonymousRecord(userId: user._id, workerId: coworker.worker?._id ?? "", recordId: self.data?._id ?? "", onSuccess: { (response) in
                switch response.error {
                case 0:
                    self.coworker = nil
                    self.delegate?.showSuccess(message: "Salida de Empleado registrada.")
                case 2:
                    self.delegate?.showError(message: "Número de empleado no existe.")
                case 3:
                    self.delegate?.showError(message: "No existe registro para terminar.")
                default:
                    self.delegate?.showError(message: "Error de servidor.")
                }
            }) { (error) in
                self.delegate?.showError(message: error.localizedDescription)
            }
        }
    }
    
    func invalidate() {
        useCase.invalidate(userId: user._id, workerId: user.worker?._id ?? "", recordId: data?._id ?? "", onSuccess: { (response) in
            switch response.error {
            case 0:
                self.delegate?.success()
                RecordService.shared.record = nil
                self.goToHome()
            case 1:
                self.delegate?.showError(message: "Error de servidor")
            case 2:
                self.delegate?.showError(message: "Número de empleado no existe")
            case 3:
                self.delegate?.showError(message: "No existe registro para terminar")
                self.goToHome()
            default:
                self.delegate?.showError(message: "Error de servidor")
            }
        }) { (error) in
            self.delegate?.showError(message: error.localizedDescription)
        }
        
    }
}
