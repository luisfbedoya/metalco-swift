//
//  ResourceMenuViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 08/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

class ResourceMenuViewController: UIViewController {
    @IBOutlet weak var warehouseButton: UIButton!
    @IBOutlet weak var resourcesButton: UIButton!
    var viewModel: ResourceMenuViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.fetchWareHouses()
        // Do any additional setup after loading the view.
    }
    @IBAction func backClick(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func warehouseClick(_ sender: Any) {
        viewModel.toWarehouse()
    }
    
    @IBAction func resourcesClick(_ sender: Any) {
        viewModel.toResources()
    }
}
