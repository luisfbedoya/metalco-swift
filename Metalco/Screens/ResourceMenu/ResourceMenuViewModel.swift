//
//  ResourceMenuViewModel.swift
//  Metalco
//
//  Created by Felipe Bedoya on 08/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public class ResourceMenuViewModel {
    
    let warehouseUseCase: WarehouseUseCase
    let navigator: ResourceMenuNavigator
    var warehouse: Warehouse? = nil
    
    init(warehouseUseCase: WarehouseUseCase, navigator: ResourceMenuNavigator) {
        self.warehouseUseCase = warehouseUseCase
        self.navigator = navigator
    }
    
    func fetchWareHouses() {
        warehouseUseCase.warehouses(workerId: UserService.shared.user?.worker?._id ?? "", onSuccess: { (warehouse) in
            self.warehouse = warehouse
            if self.warehouse == nil {
                self.toResources()
            }
        }) { (err) in
            
        }
    }
    
    func toResources() {
        navigator.toResources()
    }
    
    func toWarehouse() {
        if let warehouse = self.warehouse {
            navigator.toWarehouse(warehouse: warehouse)
        }
    }
}
