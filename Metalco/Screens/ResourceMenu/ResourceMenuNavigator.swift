//
//  ResourceMenuNavigator.swift
//  Metalco
//
//  Created by Felipe Bedoya on 08/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import SwinjectStoryboard

public class ResourceMenuNavigator {
    let storyboard: SwinjectStoryboard
    
    init(storyboard: SwinjectStoryboard) {
        self.storyboard = storyboard
    }
    
    func toWarehouse(warehouse: Warehouse) {
        let vc = storyboard.instantiateViewController(withIdentifier: "WarehouseViewController") as! WarehouseViewController
        vc.viewModel.warehouse = warehouse
        let tabBarController = UIApplication.shared.windows.first?.rootViewController as! TabBarViewController
        (tabBarController.selectedViewController as? MainNavigationViewController)?.pushViewController(vc, animated: true)
    }
    
    func toResources() {
        let vc = storyboard.instantiateViewController(withIdentifier: "ResourcesViewController") as! ResourcesViewController
        let tabBarController = UIApplication.shared.windows.first?.rootViewController as! TabBarViewController
        (tabBarController.selectedViewController as? MainNavigationViewController)?.pushViewController(vc, animated: true)
    }
}
