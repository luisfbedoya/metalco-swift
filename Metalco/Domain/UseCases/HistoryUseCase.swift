//
//  HistoryUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol HistoryUseCase {
    func fetch(userId: String, workerId: String, onSuccess: (([Record]) -> Void)?, onError: ((NetworkError) -> Void)?)
}
