//
//  ProfileUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 05/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol ProfileUseCase {
    func fetch(userId: String, onSuccess: ((Data) -> Void)?, onError: ((NetworkError) -> Void)?)
    func changePassword(userId: String, currentPassword:String, newPassword: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func uploadPhoto(userId: String, photo: Data, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
}
