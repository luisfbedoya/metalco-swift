//
//  LocationValidation.swift
//  Metalco
//
//  Created by Felipe Bedoya on 16/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol LocationValidationUseCase {
    func detectFace(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
}
