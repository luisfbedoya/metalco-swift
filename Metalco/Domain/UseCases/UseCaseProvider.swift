//
//  UseCaseProvider.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol UseCaseProvider {
    func makeAuthenticationUseCase() -> AuthenticationUseCase
    func makeHistoryUseCase() -> HistoryUseCase
    func makeProfileUseCase() -> ProfileUseCase
    func makeWorkplaceUseCase() -> WorkplaceUseCase
    func makeLocationValidationUseCase() -> LocationValidationUseCase
    func makeDashboardUseCase() -> DashboardUseCase
}
