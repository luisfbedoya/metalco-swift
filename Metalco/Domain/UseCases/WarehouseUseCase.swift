//
//  WarehouseUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 08/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol WarehouseUseCase {
    func fetch(onSuccess: (([Warehouse]) -> Void)?, onError: ((NetworkError) -> Void)?)
    func warehouses(workerId: String, onSuccess: ((Warehouse?) -> Void)?, onError: ((NetworkError) -> Void)?)
}
