//
//  ResourceUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 13/12/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public protocol ResourceUseCase {
    func resourcesByWarehouse(warehouseId: String, onSuccess: (([Resource]) -> Void)?, onError: ((NetworkError) -> Void)?)
    func resourcesByWorker(workerId: String, onSuccess: (([Resource]) -> Void)?, onError: ((NetworkError) -> Void)?)
    func report(workerId: String, resourceId: String, reason: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func transfer(workerId: String, warehouseId: String, resourceId: String, fromType: String, toType: String, toWorkerId: String, toWarehouseId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func update(workerId: String, resourceId: String, money: Int, updatedValue: Double, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func maintenance(workerId: String, warehouseId: String, resourceId: String, reason: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func cancelMaintenance(resourceId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func finishMaintenance(resourceId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func findById(resourceId: String, workerId: String, latitude: Double, longitude: Double, onSuccess: (([Resource]) -> Void)?, onError: ((NetworkError) -> Void)?)
}
