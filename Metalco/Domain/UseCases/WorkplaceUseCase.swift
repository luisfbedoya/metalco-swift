//
//  WorkplaceUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 11/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol WorkplaceUseCase {
    func fetch(userId: String, workerId: String, onSuccess: (([Workplace]) -> Void)?, onError: ((NetworkError) -> Void)?)
    func validateManager(workerId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
}
