//
//  DashboardUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

protocol DashboardUseCase {
    func fetchRecord(userId: String, workerId: String, recordId: String, onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?)
    func createRecord(userId: String, workerId: String, workplaceId: String, onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?)
    func finishRecord(userId: String, workerId: String, recordId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func checkInBreak(userId: String, workerId: String, recordId: String, onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?)
    func checkOutBreak(userId: String, workerId: String, recordId: String, onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?)
    func createAnonymousRecord(userId: String, workerId: String, workplaceId: String,onSuccess: ((Record) -> Void)?, onError: ((NetworkError) -> Void)?)
    func finishAnonymousRecord(userId: String, workerId: String, recordId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func validateFace(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func validateAnonymousUser(username: String, onSuccess: ((ProfileData) -> Void)?, onError: ((NetworkError) -> Void)?)
    func uploadFace(userId: String, data: Data, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func invalidate(userId: String, workerId: String, recordId: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
}
