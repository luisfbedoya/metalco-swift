//
//  AuthenticationUseCase.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public protocol AuthenticationUseCase {
    func login(username: String, password: String, token: String, onSuccess: ((User?) -> Void)?, onError: ((NetworkError) -> Void)?)
    func register(username: String, password: String, workerCode: String, onSuccess: ((ResponseError) -> Void)?, onError: ((NetworkError) -> Void)?)
    func fetch(userId: String, token: String, onSuccess: ((ValidationResponse) -> Void)?, onError: ((NetworkError) -> Void)?)
}
