//
//  Resource.swift
//  Metalco
//
//  Created by Felipe Bedoya on 29/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public enum ResourceStatus: String, Codable, Comparable {
    case ACTIVE = "ACTIVE"
    case INACTIVE = "INACTIVE"
    case INACTIVE_APPLICATION = "INACTIVE_APPLICATION"
    case MAINTENANCE = "MAINTENANCE"
    case PENDING_TRANSFER = "PENDING_TRANSFER"
    
    public static func < (lhs: ResourceStatus, rhs: ResourceStatus) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}

public enum  ResourceTypeMaintenance: String, Codable, Comparable {
    case NONE = "NONE"
    case HOROMETER = "HOROMETER"
    case KILOMETER = "KILOMETER"
    case TIME = "TIME"
    
    public static func < (lhs: ResourceTypeMaintenance, rhs: ResourceTypeMaintenance) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
    
}


public struct Resource: Codable {
    public let _id: String
    public let name: String
    public let family: String
    public let subFamily: String
    public let description: String
    public let identifierNumber: String
    public let tagNumber: String
    public let brand: String
    public let model: String
    public let serie: String
    public let invoiceNumber: String
//    public let date: Date
//    public let createdAt: Date
//    public let updatedAt: Date
//    public let parametersUpdatedAt: Date
    public let cost: Int
    public let maintenanceNumber: Double?
    public let maintenanceFrequency: Double?
    public let currentMaintenance: Double?
    public let worker: Worker?
    public let warehouse: Warehouse?
    public var typeMaintenance: ResourceTypeMaintenance
    public var status: ResourceStatus
    public let typeResource: String
    public let typeGas: String
    public let location: String
    
    public var typeMaintenanceString: String {
        switch typeMaintenance {
            case .HOROMETER:
                return "Horómetro"
            case .NONE:
                return "Ninguno"
            case .KILOMETER:
                return "Kilómetro"
            case .TIME:
                return "Tiempo"
        }
    }
    
    public var statusString: String {
        switch status {
            case .ACTIVE:
                return "Operativo"
            case .INACTIVE:
                return "Baja"
            case .INACTIVE_APPLICATION:
                return "Solicitud de baja"
            case .MAINTENANCE:
                return "Mantenimiento"
            case .PENDING_TRANSFER:
                return "Transferencia pendiente"
        }
    }
}
