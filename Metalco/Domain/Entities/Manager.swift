//
//  Manager.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import RealmSwift

public class Manager: Object, Codable  {
    @objc dynamic var _id: String
    @objc dynamic var code: Int
    @objc dynamic var firstName: String
    @objc dynamic var fatherLastName: String
    @objc dynamic var motherLastName: String
}

extension Manager {
    var fullname: String {
        return "\(firstName) \(fatherLastName) \(motherLastName)"
    }
}
