//
//  Workspace.swift
//  Metalco
//
//  Created by Felipe Bedoya on 14/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import RealmSwift

public class Workspace: Object, Codable {
    @objc dynamic var _id: String
    @objc dynamic var coordinates: String?
    @objc dynamic var latitude: Double
    @objc dynamic var longitude: Double
    @objc dynamic var radiusMeters: Int
}
