//
//  Record.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import RealmSwift

public class Record: Object, Codable {
    @objc dynamic var  _id: String
    @objc dynamic var inDate: Date?
    @objc dynamic var outDate: Date?
    @objc dynamic var date: Date?
    @objc dynamic var inBreakDate: Date?
    @objc dynamic var outBreakDate: Date?
    @objc dynamic var manager: Manager?
    @objc dynamic var worker: Worker?
    @objc dynamic var workPlace: Workplace?
    @objc dynamic var status: String?
    
    private enum CodingKeys: String, CodingKey {
        case _id
        case inDate
        case outDate
        case date
        case inBreakDate
        case outBreakDate
        case manager
        case worker
        case workPlace
        case status
    }
    
    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self._id = try container.decode(String.self, forKey: ._id)
        self.manager = try? container.decodeIfPresent(Manager.self, forKey: .manager) ?? nil
        self.worker = try? container.decodeIfPresent(Worker.self, forKey: .worker) ?? nil
        self.workPlace = try? container.decodeIfPresent(Workplace.self, forKey: .workPlace) ?? nil
        self.inDate = Date.milisToTime(milis: try? container.decodeIfPresent(Double.self, forKey: .inDate) ?? nil)
        self.outDate = Date.milisToTime(milis: try? container.decodeIfPresent(Double.self, forKey: .outDate) ??  nil)
        self.inBreakDate = Date.milisToTime(milis: try? container.decodeIfPresent(Double.self, forKey: .inBreakDate) ??  nil)
        self.outBreakDate = Date.milisToTime(milis: try? container.decodeIfPresent(Double.self, forKey: .outBreakDate) ?? nil)
        let dateString = try container.decodeIfPresent(String.self, forKey: .date) ?? ""
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        self.date = dateFormatter.date(from: dateString)!
        self.status = try container.decode(String.self, forKey: .status)
    }
    
    required init() {
        self._id = ""
        self.manager = nil
        self.worker = nil
        self.workPlace = nil
        self.inDate = nil
        self.outDate = nil
        self.inBreakDate = nil
        self.outBreakDate = nil
        self.date = nil
        self.status = nil
    }
}
