//
//  Warehouse.swift
//  Metalco
//
//  Created by Felipe Bedoya on 08/11/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

public struct Warehouse: Codable {
    public let _id: String
    public let name: String
    public let description: String
    public let address: String
    public let phoneNumber: String
    public let workSpace: Workspace
    public let responsable: Worker
    public let manager: Manager
}
