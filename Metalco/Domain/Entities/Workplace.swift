//
//  Workplace.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import RealmSwift

public class Workplace: Object, Codable {
    @objc dynamic var  _id: String
    @objc dynamic var  name: String
    @objc dynamic var  address: String
    @objc dynamic var  phoneNumber: String
    @objc dynamic var  workSpace: Workspace?
    @objc dynamic var  mainManager: Manager?
}
