//
//  User.swift
//  Metalco
//
//  Created by Felipe Bedoya on 01/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import RealmSwift

public class User: Object, Codable {
    @objc dynamic var _id: String
    @objc dynamic var username: String
    @objc dynamic var password: String
    @objc dynamic var type: String
    @objc dynamic var profilePicture: String?
    @objc dynamic var workerNumber: String?
    @objc dynamic var worker: Worker?
}
