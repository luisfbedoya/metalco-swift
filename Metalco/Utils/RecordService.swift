//
//  RecordService.swift
//  Metalco
//
//  Created by Felipe Bedoya on 18/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

public class RecordService {
    static let shared: RecordService = RecordService()

    var record: Record? {
        didSet {
            saveToRealm()
        }
    }
    
    private init() {
        let realm = try! Realm(configuration: Constants.RMConfig().config)
        if let record = realm.objects(Record.self).first {
            self.record = record
        }
    }
    
    private func saveToRealm() {
        deleteCurrentRecord()
        let realm = try! Realm(configuration: Constants.RMConfig().config)
        try! realm.write({
            if let record = self.record {
                realm.add(record)
            }
        })
    }
    
    private func deleteCurrentRecord() {
        let realm = try! Realm(configuration: Constants.RMConfig().config)
        try! realm.write({
            if let savedRecord = realm.objects(Record.self).first {
                realm.delete(savedRecord)
            }
        })
    }
}
