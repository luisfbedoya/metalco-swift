//
//  UIButton.swift
//  Metalco
//
//  Created by Felipe Bedoya on 05/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit

@IBDesignable extension UIButton {

    @IBInspectable override var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable override var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable override var borderColor: UIColor {
        set {
            layer.borderColor = newValue.cgColor
        }
        get {
            guard let color = layer.borderColor else { return UIColor() }
            return UIColor(cgColor: color)
            
        }
    }
}
