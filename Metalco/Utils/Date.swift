//
//  Date.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation

extension Date {
    func toString(like format: String) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = format
        return dateFormatterPrint.string(from: self)
    }
    
    static func milisToTime(milis: Double?) -> Date? {
        if let milis = milis {
            return Date(timeIntervalSince1970: milis/1000)
        } else {
            return nil
        }
    }
}
