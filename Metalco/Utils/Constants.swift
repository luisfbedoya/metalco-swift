//
//  Constants.swift
//  Metalco
//
//  Created by Felipe Bedoya on 13/12/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import RealmSwift

public struct Constants {
    public struct RMConfig {
        public let config: Realm.Configuration = Realm.Configuration(schemaVersion: 2, migrationBlock: { (migration, oldSchemaVersion) in
            if oldSchemaVersion < 1 {
                migration.enumerateObjects(ofType: "Workspace") { (old, new) in
                    
                }
            }
        })
    }
}
