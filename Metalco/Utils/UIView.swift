//
//  UIView.swift
//  Metalco
//
//  Created by Felipe Bedoya on 07/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit
@IBDesignable extension UIView{

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor {
        set {
            layer.borderColor = newValue.cgColor
        }
        get {
            guard let color = layer.borderColor else { return UIColor() }
            return UIColor(cgColor: color)
        }
    }
}
