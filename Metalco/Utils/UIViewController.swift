//
//  UIViewController.swift
//  Metalco
//
//  Created by Felipe Bedoya on 17/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable {
    func embed(view: UIView, into otherView: UIView) {
        otherView.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: otherView.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: otherView.trailingAnchor).isActive = true
        view.heightAnchor.constraint(equalTo: otherView.heightAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: otherView.centerYAnchor).isActive = true
    }
    
    func showMessage(title: String, message: String, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showPermissionErrorMessage(title: String, message: String) {
        showMessage(title: title, message: message) { (action) in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in })
             }
        }
    }

    func showAnimation() {
        self.startAnimating(type: NVActivityIndicatorType.circleStrokeSpin)
    }
    
    func stopAnimation() {
        self.stopAnimating()
    }
    
    @objc func didTapOutside() {
        self.dismiss(animated: true, completion: nil)
    }
}
