//
//  UserService.swift
//  Metalco
//
//  Created by Felipe Bedoya on 03/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

public class UserService {
    static let shared: UserService = UserService()
    
    var user: User? {
        didSet {
            saveToRealm()
        }
    }
    
    private init() {
        let realm = try! Realm(configuration: Constants.RMConfig().config)
        if let user = realm.objects(User.self).first {
            self.user = user
        }
    }
    
    private func saveToRealm() {
        deleteCurrentUser()
        let realm = try! Realm(configuration: Constants.RMConfig().config)
        try! realm.write({
            if let user = self.user {
                realm.add(user)
            }
        })
    }
    
    func deleteCurrentUser() {
        let realm = try! Realm(configuration: Constants.RMConfig().config)
        try! realm.write({
            if let savedUser = realm.objects(User.self).first {
                realm.delete(savedUser)
            }
        })
    }
}
