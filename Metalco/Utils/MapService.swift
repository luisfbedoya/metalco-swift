//
//  MapService.swift
//  Metalco
//
//  Created by Felipe Bedoya on 14/05/20.
//  Copyright © 2020 Felipe Bedoya. All rights reserved.
//

import Foundation
import GoogleMaps
import MapKit

protocol MapServiceDelegate {
    func didAuthorizeLocation()
    func didUpdateLocation(location: CLLocation)
    func didNotAuthorizeLocation()
}

public class MapService: NSObject {
    static let shared: MapService = MapService()
    var delegate: MapServiceDelegate? = nil
    var locationManager: CLLocationManager? = nil
    var currentLocation: CLLocation? = nil {
        didSet {
            if let location = currentLocation {
                delegate?.didUpdateLocation(location: location)
            }
        }
    }
    
    private override init() {
        super.init()
    }
        
    func navigateTo(latitude: Double, longitude: Double) {
        if UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!) {
            UIApplication.shared.open(NSURL(string:
                "comgooglemaps://?saddr=&daddr=\(latitude),\(longitude)&directionsmode=driving")! as URL)
        } else {
            let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude)))
            destination.name = "Destination"
            MKMapItem.openMaps(with: [destination], launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
        }
    }
    
    func isInside(workspace: Workspace) -> Bool {
        let location = CLLocation(latitude: workspace.latitude, longitude: workspace.longitude)
        return currentLocation?.distance(from: location) ?? Double(workspace.radiusMeters + 1) < Double(workspace.radiusMeters)
    }
    
    func requestCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
    }
}

extension MapService: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            self.delegate?.didNotAuthorizeLocation()
            return
        }
        
        guard status == .authorizedWhenInUse else {
            manager.requestWhenInUseAuthorization()
            return
        }
        manager.startUpdatingLocation()
        delegate?.didAuthorizeLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        currentLocation = location
        manager.stopUpdatingLocation()
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
    
    
}
